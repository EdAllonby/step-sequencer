﻿using System;
using System.Windows.Forms;

namespace Step_Sequencer
{
    public partial class StepSequencer : Form
    {
        private readonly Board.Board sequencer;

       
        public StepSequencer()
        {
            //Add whatever controls are on the from into a ControlCollection
            var formControls = Controls;

            //Pass these controls into board to do logic on.
            sequencer = new Board.Board(formControls);

            InitializeComponent();
            sequencer.InitialiseElements();
        }

        /// <summary>
        /// Runs every beat set by the user
        /// </summary>
        private void HeartBeat(object sender, EventArgs e)
        {
            sequencer.NewBeat();
        }

        private void speed_ValueChanged(object sender, EventArgs e)
        {
           beatTicker.Interval = speed.Value;
        }

    }
}