﻿using System;
using System.Windows.Forms;

namespace Step_Sequencer.fmodEx
{
    internal static class FmodUtilities
    {

        public static fmodEx.System System = null;
        public static Channel Channel = null;

        public static void Errcheck(RESULT result)
        {
            if (result != RESULT.OK)
            {
                MessageBox.Show(string.Format("FMOD error! {0} - {1}", result, Error.String(result)));
                Environment.Exit(-1);
            }
        }

        public static void DisposeMemory()
        {
            if (System != null)
            {
                var result = System.close();
                Errcheck(result);
                result = System.release();
                Errcheck(result);
            }
        }

        public static void OnFormLoad(object sender, EventArgs e)
        {
            uint version = 0;

            var result = Factory.System_Create(ref System);
            Errcheck(result);
            result = System.getVersion(ref version);
            Errcheck(result);

            if (version < VERSION.number)
            {
                MessageBox.Show(string.Format("Error!  You are using an old version of FMOD {0}." +
                                              "\nThis program requires {1}.", version.ToString("X"),
                    VERSION.number.ToString("X")));

                Application.Exit();
            }
            result = System.init(32, INITFLAGS.NORMAL, (IntPtr) null);
            Errcheck(result);
        }
    }
}