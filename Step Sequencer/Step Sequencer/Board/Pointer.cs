﻿using System.Drawing;
using System.Windows.Forms;

namespace Step_Sequencer.Board
{
    public class Pointer : IElement
    {
        public PictureBox PointerInstance = new PictureBox();

        public bool IsElementOn { get; set; }
        public bool IsLowerElement;

        public Pointer()
        {
            IsElementOn = false;
            IsLowerElement = false;
        }

        public void SetUpPictureBox()
        {
            PointerInstance.BackColor = Color.Transparent;
            InitialiseElements();
        }

        public void InitialiseElements()
        {
            ElementPreviousBeat();
        }

        public void ElementSelectedBeat()
        {
            if (IsLowerElement)
            {
                PointerInstance.Image = Properties.Resources.pointerOnUp;
            }
            else
            {
                PointerInstance.Image = Properties.Resources.pointerOnDown;
            }
        }

        public void ElementPreviousBeat()
        {
            if (IsLowerElement)
            {
                PointerInstance.Image = Properties.Resources.pointerOffUp;
            }
            else
            {
                PointerInstance.Image = Properties.Resources.pointerOffDown;
            }        
        }
    }
}