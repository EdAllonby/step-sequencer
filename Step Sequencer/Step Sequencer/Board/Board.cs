﻿using System;
using System.Linq;
using System.Windows.Forms;
using LEDLight;

namespace Step_Sequencer.Board
{
    public class Board
    {
        private const int CellRows = 12;
        public static int Bars = 16;

        public Cell[,] CellArray = new Cell[Bars, CellRows];
        public Pointer[,] PointerArray = new Pointer[Bars, 2];
        public Led[] LedArray = new Led[CellRows];
        public int CurrentBar;

        //Used when setting references to controls on form
        private readonly Control.ControlCollection controlList;

        public Board(Control.ControlCollection controlList)
        {
            this.controlList = controlList;

            //Starting bar is nothing
            CurrentBar = -1;
        }

        public void InitialiseElements()
        {
            InitialiseCells();
            InitialisePointers();
            InitialiseLeds();
        }

        public void InitialiseCells()
        {
            var xCellCounter = 0;
            var yCellCounter = 0;
            foreach (
                var cell in
                    controlList.OfType<PictureBox>()
                        .Where(x => x.Name.Contains("cell"))
                        .OrderBy(x => Int32.Parse(x.Name.Remove(0, 4))))
            {
                CellArray[xCellCounter, yCellCounter] = new Cell {CellInstance = cell};

                xCellCounter++;
                if (xCellCounter == Bars)
                {
                    yCellCounter++;
                    xCellCounter = 0;
                }
            }


            foreach (var cell in CellArray)
            {
                cell.SetUpPictureBox();
            }
        }

        public void InitialisePointers()
        {
            var xPointerCounter = 0;
            var yPointerCounter = 0;
            foreach (
                var pointer in  
                    controlList.OfType<PictureBox>()
                        .Where(x => x.Name.Contains("pointer"))
                        .OrderBy(x => Int32.Parse(x.Name.Remove(0, 7))))
            {
                PointerArray[xPointerCounter, yPointerCounter] = new Pointer {PointerInstance = pointer};

                xPointerCounter++;

                if (xPointerCounter == Bars)
                {
                    yPointerCounter++;
                    xPointerCounter = 0;
                }
            }

            for (var i = 0; i < Bars; i++)
            {
                PointerArray[i, 0].IsLowerElement = false;
                PointerArray[i, 0].SetUpPictureBox();

                PointerArray[i, 1].IsLowerElement = true;
                PointerArray[i, 1].SetUpPictureBox();
            }
        }

        private void InitialiseLeds()
        {
            var ledRowCounter = 0;

            foreach (
                var led in  
                    controlList.OfType<LedBulb>()
                        .Where(x => x.Name.Contains("ledBulb"))
                        .OrderBy(x => Int32.Parse(x.Name.Remove(0, 7))))
            {
                LedArray[ledRowCounter] = new Led {LedInstance = led};
                ledRowCounter++;
            }
        }

        public void NewBeat()
        {
            IterateCounter();

            //Pointer for loop
            for(var i = 0; i < 2; i++)
            {
                //turn on current pointer column
                PointerArray[CurrentBar, i].ElementSelectedBeat();

                //turn off previous pair column
                PointerArray[Mod(CurrentBar - 1, Bars), i].ElementPreviousBeat();
            }
            for (var i = 0; i < CellRows; i++)
            {
                //Turn on current cell column 
                CellArray[CurrentBar, i].ElementSelectedBeat();

                //Turn off previous cell column
                CellArray[Mod(CurrentBar - 1, Bars), i].ElementPreviousBeat();
            }

            //Turn On/Off LEDs
            for (var row = 0; row < CellRows; row++)
            {
                if (CellArray[CurrentBar, row].IsElementOn)
                {
                    LedArray[row].TurnOn();
                }
                    // If the LED is turned on and element is on then turn LED off,
                    // If the LED is already turned off, then there is no need to run the TurnOffMethod
                else if (!CellArray[CurrentBar, row].IsElementOn && LedArray[row].LedOn)
                {
                    LedArray[row].TurnOff();
                }
            }
        }

        //Adds one to the counter and if it reaches the totals bars, wrap to 0
        private void IterateCounter()
        {
            CurrentBar = (CurrentBar + 1)%Bars;
        }

        //Modulus function with negative number functionality
        public int Mod(int x, int m)
        {
            return (x%m + m)%m;
        }
    }
}