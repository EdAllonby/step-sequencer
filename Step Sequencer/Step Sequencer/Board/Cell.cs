﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Step_Sequencer.Board
{
    public class Cell : IElement
    {
        public PictureBox CellInstance = new PictureBox();

        public bool IsElementOn { get; set; }

        public void SetUpPictureBox()
        {
            CellInstance.MouseDown += CellInstanceClick;
            CellInstance.BackColor = Color.Transparent;
            InitialiseElements();
        }

        public void InitialiseElements()
        {
            ElementPreviousBeat();
        }

        private void CellInstanceClick(object sender, EventArgs e)
        {
            CellClicked();
        }

        /// <summary>
        /// This method is called when a cell is clicked. The clicked cell will change state
        /// from On/Off and set it's IsElementOn flag accordingly
        /// </summary>
        public void CellClicked()
        {
            if (!IsElementOn)
            {
                CellInstance.Image = Properties.Resources.cellOnUnselected;
                IsElementOn = true;
            }
            else
            {
                CellInstance.Image = Properties.Resources.cellOffUnselected;
                IsElementOn = false;
            }
        }

        /// <summary>
        /// Turns column of cells on based on its current state (IsElementOn)
        /// </summary>
        public void ElementSelectedBeat()
        {
            CellInstance.Image = IsElementOn
                ? Properties.Resources.cellOnSelected
                : Properties.Resources.CellOffSelected;
        }

        /// <summary>
        /// Turns column of cells off based on it's current state (IsElementOn)
        /// </summary>
        public void ElementPreviousBeat()
        {
            CellInstance.Image = IsElementOn
                ? Properties.Resources.cellOnUnselected
                : Properties.Resources.cellOffUnselected;
        }
    }
}