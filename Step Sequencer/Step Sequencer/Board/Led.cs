﻿using System.Drawing;
using LEDLight;

namespace Step_Sequencer.Board
{
    public class Led
    {
        public LedBulb LedInstance = new LedBulb();
        public bool LedOn = false;

        public void TurnOn()
        {
            LedInstance.Color = Color.Red;
            LedOn = true;
        }

        public void TurnOff()
        {
            LedInstance.Color = Color.LightGreen;
            LedOn = false;
        }
    }
}
