﻿using Step_Sequencer.fmodEx;

namespace Step_Sequencer.Board
{
    internal class Audio
    {
        private RESULT result;

        private DSP dsp;
        public float Frequency = 440;

        public void PlaySound()
        {
            result = FmodUtilities.System.createDSPByType(DSP_TYPE.OSCILLATOR, ref dsp);
            FmodUtilities.Errcheck(result);
            result = dsp.setParameter((int)DSP_OSCILLATOR.RATE, Frequency); /* musical note 'A' */
            FmodUtilities.Errcheck(result);
        }
    }
}