﻿namespace Step_Sequencer.Board
{
    public interface IElement
    {
        bool IsElementOn { get; set; }

        void SetUpPictureBox();

        void ElementSelectedBeat();

        void ElementPreviousBeat();

        void InitialiseElements();
    }
}