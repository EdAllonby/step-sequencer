﻿using System.Drawing;
using LEDLight;
using Step_Sequencer.fmodEx;

namespace Step_Sequencer
{
    partial class StepSequencer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// When Closing
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            if (disposing)
            {
                FmodUtilities.DisposeMemory();

                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cell7 = new System.Windows.Forms.PictureBox();
            this.cell8 = new System.Windows.Forms.PictureBox();
            this.cell16 = new System.Windows.Forms.PictureBox();
            this.cell15 = new System.Windows.Forms.PictureBox();
            this.cell9 = new System.Windows.Forms.PictureBox();
            this.cell14 = new System.Windows.Forms.PictureBox();
            this.cell10 = new System.Windows.Forms.PictureBox();
            this.cell13 = new System.Windows.Forms.PictureBox();
            this.cell11 = new System.Windows.Forms.PictureBox();
            this.cell12 = new System.Windows.Forms.PictureBox();
            this.cell4 = new System.Windows.Forms.PictureBox();
            this.cell5 = new System.Windows.Forms.PictureBox();
            this.cell6 = new System.Windows.Forms.PictureBox();
            this.cell3 = new System.Windows.Forms.PictureBox();
            this.cell2 = new System.Windows.Forms.PictureBox();
            this.cell32 = new System.Windows.Forms.PictureBox();
            this.cell31 = new System.Windows.Forms.PictureBox();
            this.cell30 = new System.Windows.Forms.PictureBox();
            this.cell29 = new System.Windows.Forms.PictureBox();
            this.cell28 = new System.Windows.Forms.PictureBox();
            this.cell27 = new System.Windows.Forms.PictureBox();
            this.cell26 = new System.Windows.Forms.PictureBox();
            this.cell25 = new System.Windows.Forms.PictureBox();
            this.cell24 = new System.Windows.Forms.PictureBox();
            this.cell23 = new System.Windows.Forms.PictureBox();
            this.cell22 = new System.Windows.Forms.PictureBox();
            this.cell21 = new System.Windows.Forms.PictureBox();
            this.cell20 = new System.Windows.Forms.PictureBox();
            this.cell19 = new System.Windows.Forms.PictureBox();
            this.cell18 = new System.Windows.Forms.PictureBox();
            this.cell48 = new System.Windows.Forms.PictureBox();
            this.cell47 = new System.Windows.Forms.PictureBox();
            this.cell46 = new System.Windows.Forms.PictureBox();
            this.cell45 = new System.Windows.Forms.PictureBox();
            this.cell44 = new System.Windows.Forms.PictureBox();
            this.cell43 = new System.Windows.Forms.PictureBox();
            this.cell42 = new System.Windows.Forms.PictureBox();
            this.cell41 = new System.Windows.Forms.PictureBox();
            this.cell40 = new System.Windows.Forms.PictureBox();
            this.cell39 = new System.Windows.Forms.PictureBox();
            this.cell38 = new System.Windows.Forms.PictureBox();
            this.cell37 = new System.Windows.Forms.PictureBox();
            this.cell36 = new System.Windows.Forms.PictureBox();
            this.cell35 = new System.Windows.Forms.PictureBox();
            this.cell34 = new System.Windows.Forms.PictureBox();
            this.cell64 = new System.Windows.Forms.PictureBox();
            this.cell63 = new System.Windows.Forms.PictureBox();
            this.cell62 = new System.Windows.Forms.PictureBox();
            this.cell61 = new System.Windows.Forms.PictureBox();
            this.cell60 = new System.Windows.Forms.PictureBox();
            this.cell59 = new System.Windows.Forms.PictureBox();
            this.cell58 = new System.Windows.Forms.PictureBox();
            this.cell57 = new System.Windows.Forms.PictureBox();
            this.cell56 = new System.Windows.Forms.PictureBox();
            this.cell55 = new System.Windows.Forms.PictureBox();
            this.cell54 = new System.Windows.Forms.PictureBox();
            this.cell53 = new System.Windows.Forms.PictureBox();
            this.cell52 = new System.Windows.Forms.PictureBox();
            this.cell51 = new System.Windows.Forms.PictureBox();
            this.cell50 = new System.Windows.Forms.PictureBox();
            this.cell80 = new System.Windows.Forms.PictureBox();
            this.cell79 = new System.Windows.Forms.PictureBox();
            this.cell78 = new System.Windows.Forms.PictureBox();
            this.cell77 = new System.Windows.Forms.PictureBox();
            this.cell76 = new System.Windows.Forms.PictureBox();
            this.cell75 = new System.Windows.Forms.PictureBox();
            this.cell74 = new System.Windows.Forms.PictureBox();
            this.cell73 = new System.Windows.Forms.PictureBox();
            this.cell72 = new System.Windows.Forms.PictureBox();
            this.cell71 = new System.Windows.Forms.PictureBox();
            this.cell70 = new System.Windows.Forms.PictureBox();
            this.cell69 = new System.Windows.Forms.PictureBox();
            this.cell68 = new System.Windows.Forms.PictureBox();
            this.cell67 = new System.Windows.Forms.PictureBox();
            this.cell66 = new System.Windows.Forms.PictureBox();
            this.cell96 = new System.Windows.Forms.PictureBox();
            this.cell95 = new System.Windows.Forms.PictureBox();
            this.cell94 = new System.Windows.Forms.PictureBox();
            this.cell93 = new System.Windows.Forms.PictureBox();
            this.cell92 = new System.Windows.Forms.PictureBox();
            this.cell91 = new System.Windows.Forms.PictureBox();
            this.cell90 = new System.Windows.Forms.PictureBox();
            this.cell89 = new System.Windows.Forms.PictureBox();
            this.cell88 = new System.Windows.Forms.PictureBox();
            this.cell87 = new System.Windows.Forms.PictureBox();
            this.cell86 = new System.Windows.Forms.PictureBox();
            this.cell85 = new System.Windows.Forms.PictureBox();
            this.cell84 = new System.Windows.Forms.PictureBox();
            this.cell83 = new System.Windows.Forms.PictureBox();
            this.cell82 = new System.Windows.Forms.PictureBox();
            this.cell112 = new System.Windows.Forms.PictureBox();
            this.cell111 = new System.Windows.Forms.PictureBox();
            this.cell110 = new System.Windows.Forms.PictureBox();
            this.cell109 = new System.Windows.Forms.PictureBox();
            this.cell108 = new System.Windows.Forms.PictureBox();
            this.cell107 = new System.Windows.Forms.PictureBox();
            this.cell106 = new System.Windows.Forms.PictureBox();
            this.cell105 = new System.Windows.Forms.PictureBox();
            this.cell104 = new System.Windows.Forms.PictureBox();
            this.cell103 = new System.Windows.Forms.PictureBox();
            this.cell102 = new System.Windows.Forms.PictureBox();
            this.cell101 = new System.Windows.Forms.PictureBox();
            this.cell100 = new System.Windows.Forms.PictureBox();
            this.cell99 = new System.Windows.Forms.PictureBox();
            this.cell98 = new System.Windows.Forms.PictureBox();
            this.cell128 = new System.Windows.Forms.PictureBox();
            this.cell127 = new System.Windows.Forms.PictureBox();
            this.cell126 = new System.Windows.Forms.PictureBox();
            this.cell125 = new System.Windows.Forms.PictureBox();
            this.cell124 = new System.Windows.Forms.PictureBox();
            this.cell123 = new System.Windows.Forms.PictureBox();
            this.cell122 = new System.Windows.Forms.PictureBox();
            this.cell121 = new System.Windows.Forms.PictureBox();
            this.cell120 = new System.Windows.Forms.PictureBox();
            this.cell119 = new System.Windows.Forms.PictureBox();
            this.cell118 = new System.Windows.Forms.PictureBox();
            this.cell117 = new System.Windows.Forms.PictureBox();
            this.cell116 = new System.Windows.Forms.PictureBox();
            this.cell115 = new System.Windows.Forms.PictureBox();
            this.cell114 = new System.Windows.Forms.PictureBox();
            this.cell144 = new System.Windows.Forms.PictureBox();
            this.cell143 = new System.Windows.Forms.PictureBox();
            this.cell142 = new System.Windows.Forms.PictureBox();
            this.cell141 = new System.Windows.Forms.PictureBox();
            this.cell140 = new System.Windows.Forms.PictureBox();
            this.cell139 = new System.Windows.Forms.PictureBox();
            this.cell138 = new System.Windows.Forms.PictureBox();
            this.cell137 = new System.Windows.Forms.PictureBox();
            this.cell136 = new System.Windows.Forms.PictureBox();
            this.cell135 = new System.Windows.Forms.PictureBox();
            this.cell134 = new System.Windows.Forms.PictureBox();
            this.cell133 = new System.Windows.Forms.PictureBox();
            this.cell132 = new System.Windows.Forms.PictureBox();
            this.cell131 = new System.Windows.Forms.PictureBox();
            this.cell130 = new System.Windows.Forms.PictureBox();
            this.cell160 = new System.Windows.Forms.PictureBox();
            this.cell159 = new System.Windows.Forms.PictureBox();
            this.cell158 = new System.Windows.Forms.PictureBox();
            this.cell157 = new System.Windows.Forms.PictureBox();
            this.cell156 = new System.Windows.Forms.PictureBox();
            this.cell155 = new System.Windows.Forms.PictureBox();
            this.cell154 = new System.Windows.Forms.PictureBox();
            this.cell153 = new System.Windows.Forms.PictureBox();
            this.cell152 = new System.Windows.Forms.PictureBox();
            this.cell151 = new System.Windows.Forms.PictureBox();
            this.cell150 = new System.Windows.Forms.PictureBox();
            this.cell149 = new System.Windows.Forms.PictureBox();
            this.cell148 = new System.Windows.Forms.PictureBox();
            this.cell147 = new System.Windows.Forms.PictureBox();
            this.cell146 = new System.Windows.Forms.PictureBox();
            this.cell176 = new System.Windows.Forms.PictureBox();
            this.cell175 = new System.Windows.Forms.PictureBox();
            this.cell174 = new System.Windows.Forms.PictureBox();
            this.cell173 = new System.Windows.Forms.PictureBox();
            this.cell172 = new System.Windows.Forms.PictureBox();
            this.cell171 = new System.Windows.Forms.PictureBox();
            this.cell170 = new System.Windows.Forms.PictureBox();
            this.cell169 = new System.Windows.Forms.PictureBox();
            this.cell168 = new System.Windows.Forms.PictureBox();
            this.cell167 = new System.Windows.Forms.PictureBox();
            this.cell166 = new System.Windows.Forms.PictureBox();
            this.cell165 = new System.Windows.Forms.PictureBox();
            this.cell164 = new System.Windows.Forms.PictureBox();
            this.cell163 = new System.Windows.Forms.PictureBox();
            this.cell162 = new System.Windows.Forms.PictureBox();
            this.cell192 = new System.Windows.Forms.PictureBox();
            this.cell191 = new System.Windows.Forms.PictureBox();
            this.cell190 = new System.Windows.Forms.PictureBox();
            this.cell189 = new System.Windows.Forms.PictureBox();
            this.cell188 = new System.Windows.Forms.PictureBox();
            this.cell187 = new System.Windows.Forms.PictureBox();
            this.cell186 = new System.Windows.Forms.PictureBox();
            this.cell185 = new System.Windows.Forms.PictureBox();
            this.cell184 = new System.Windows.Forms.PictureBox();
            this.cell183 = new System.Windows.Forms.PictureBox();
            this.cell182 = new System.Windows.Forms.PictureBox();
            this.cell181 = new System.Windows.Forms.PictureBox();
            this.cell180 = new System.Windows.Forms.PictureBox();
            this.cell179 = new System.Windows.Forms.PictureBox();
            this.cell178 = new System.Windows.Forms.PictureBox();
            this.cell177 = new System.Windows.Forms.PictureBox();
            this.cell161 = new System.Windows.Forms.PictureBox();
            this.cell145 = new System.Windows.Forms.PictureBox();
            this.cell129 = new System.Windows.Forms.PictureBox();
            this.cell113 = new System.Windows.Forms.PictureBox();
            this.cell97 = new System.Windows.Forms.PictureBox();
            this.cell81 = new System.Windows.Forms.PictureBox();
            this.cell65 = new System.Windows.Forms.PictureBox();
            this.cell49 = new System.Windows.Forms.PictureBox();
            this.cell33 = new System.Windows.Forms.PictureBox();
            this.cell17 = new System.Windows.Forms.PictureBox();
            this.cell1 = new System.Windows.Forms.PictureBox();
            this.pointer1 = new System.Windows.Forms.PictureBox();
            this.pointer2 = new System.Windows.Forms.PictureBox();
            this.pointer3 = new System.Windows.Forms.PictureBox();
            this.pointer4 = new System.Windows.Forms.PictureBox();
            this.pointer5 = new System.Windows.Forms.PictureBox();
            this.pointer6 = new System.Windows.Forms.PictureBox();
            this.pointer7 = new System.Windows.Forms.PictureBox();
            this.pointer8 = new System.Windows.Forms.PictureBox();
            this.pointer9 = new System.Windows.Forms.PictureBox();
            this.pointer10 = new System.Windows.Forms.PictureBox();
            this.pointer11 = new System.Windows.Forms.PictureBox();
            this.pointer12 = new System.Windows.Forms.PictureBox();
            this.pointer13 = new System.Windows.Forms.PictureBox();
            this.pointer14 = new System.Windows.Forms.PictureBox();
            this.pointer15 = new System.Windows.Forms.PictureBox();
            this.pointer16 = new System.Windows.Forms.PictureBox();
            this.pointer17 = new System.Windows.Forms.PictureBox();
            this.pointer18 = new System.Windows.Forms.PictureBox();
            this.pointer19 = new System.Windows.Forms.PictureBox();
            this.pointer20 = new System.Windows.Forms.PictureBox();
            this.pointer21 = new System.Windows.Forms.PictureBox();
            this.pointer22 = new System.Windows.Forms.PictureBox();
            this.pointer23 = new System.Windows.Forms.PictureBox();
            this.pointer24 = new System.Windows.Forms.PictureBox();
            this.pointer25 = new System.Windows.Forms.PictureBox();
            this.pointer26 = new System.Windows.Forms.PictureBox();
            this.pointer27 = new System.Windows.Forms.PictureBox();
            this.pointer28 = new System.Windows.Forms.PictureBox();
            this.pointer29 = new System.Windows.Forms.PictureBox();
            this.pointer30 = new System.Windows.Forms.PictureBox();
            this.pointer31 = new System.Windows.Forms.PictureBox();
            this.pointer32 = new System.Windows.Forms.PictureBox();
            this.beatTicker = new System.Windows.Forms.Timer(this.components);
            this.speed = new System.Windows.Forms.TrackBar();
            this.releaseDial = new AuSharp.Knob();
            this.knob1 = new AuSharp.Knob();
            this.waveShapeDial = new AuSharp.Knob();
            this.ledBulb1 = new LEDLight.LedBulb();
            this.ledBulb2 = new LEDLight.LedBulb();
            this.ledBulb3 = new LEDLight.LedBulb();
            this.ledBulb4 = new LEDLight.LedBulb();
            this.ledBulb5 = new LEDLight.LedBulb();
            this.ledBulb6 = new LEDLight.LedBulb();
            this.ledBulb7 = new LEDLight.LedBulb();
            this.ledBulb8 = new LEDLight.LedBulb();
            this.ledBulb9 = new LEDLight.LedBulb();
            this.ledBulb10 = new LEDLight.LedBulb();
            this.ledBulb11 = new LEDLight.LedBulb();
            this.ledBulb12 = new LEDLight.LedBulb();
            ((System.ComponentModel.ISupportInitialize)(this.cell7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell176)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell175)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell174)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell173)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell172)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell192)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell191)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell190)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell189)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell187)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell186)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell185)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell181)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell180)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell179)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell178)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell177)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speed)).BeginInit();
            this.SuspendLayout();
            // 
            // cell7
            // 
            this.cell7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell7.Location = new System.Drawing.Point(253, 74);
            this.cell7.Name = "cell7";
            this.cell7.Size = new System.Drawing.Size(20, 20);
            this.cell7.TabIndex = 6;
            this.cell7.TabStop = false;
            // 
            // cell8
            // 
            this.cell8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell8.Location = new System.Drawing.Point(287, 74);
            this.cell8.Name = "cell8";
            this.cell8.Size = new System.Drawing.Size(20, 20);
            this.cell8.TabIndex = 7;
            this.cell8.TabStop = false;
            // 
            // cell16
            // 
            this.cell16.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell16.Location = new System.Drawing.Point(559, 74);
            this.cell16.Name = "cell16";
            this.cell16.Size = new System.Drawing.Size(20, 20);
            this.cell16.TabIndex = 15;
            this.cell16.TabStop = false;
            // 
            // cell15
            // 
            this.cell15.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell15.Location = new System.Drawing.Point(525, 74);
            this.cell15.Name = "cell15";
            this.cell15.Size = new System.Drawing.Size(20, 20);
            this.cell15.TabIndex = 14;
            this.cell15.TabStop = false;
            // 
            // cell9
            // 
            this.cell9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell9.Location = new System.Drawing.Point(321, 74);
            this.cell9.Name = "cell9";
            this.cell9.Size = new System.Drawing.Size(20, 20);
            this.cell9.TabIndex = 8;
            this.cell9.TabStop = false;
            // 
            // cell14
            // 
            this.cell14.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell14.Location = new System.Drawing.Point(491, 74);
            this.cell14.Name = "cell14";
            this.cell14.Size = new System.Drawing.Size(20, 20);
            this.cell14.TabIndex = 13;
            this.cell14.TabStop = false;
            // 
            // cell10
            // 
            this.cell10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell10.Location = new System.Drawing.Point(355, 74);
            this.cell10.Name = "cell10";
            this.cell10.Size = new System.Drawing.Size(20, 20);
            this.cell10.TabIndex = 9;
            this.cell10.TabStop = false;
            // 
            // cell13
            // 
            this.cell13.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell13.Location = new System.Drawing.Point(457, 74);
            this.cell13.Name = "cell13";
            this.cell13.Size = new System.Drawing.Size(20, 20);
            this.cell13.TabIndex = 12;
            this.cell13.TabStop = false;
            // 
            // cell11
            // 
            this.cell11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell11.Location = new System.Drawing.Point(389, 74);
            this.cell11.Name = "cell11";
            this.cell11.Size = new System.Drawing.Size(20, 20);
            this.cell11.TabIndex = 10;
            this.cell11.TabStop = false;
            // 
            // cell12
            // 
            this.cell12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell12.Location = new System.Drawing.Point(423, 74);
            this.cell12.Name = "cell12";
            this.cell12.Size = new System.Drawing.Size(20, 20);
            this.cell12.TabIndex = 11;
            this.cell12.TabStop = false;
            // 
            // cell4
            // 
            this.cell4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell4.Location = new System.Drawing.Point(151, 74);
            this.cell4.Name = "cell4";
            this.cell4.Size = new System.Drawing.Size(20, 20);
            this.cell4.TabIndex = 3;
            this.cell4.TabStop = false;
            // 
            // cell5
            // 
            this.cell5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell5.Location = new System.Drawing.Point(185, 74);
            this.cell5.Name = "cell5";
            this.cell5.Size = new System.Drawing.Size(20, 20);
            this.cell5.TabIndex = 4;
            this.cell5.TabStop = false;
            // 
            // cell6
            // 
            this.cell6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell6.Location = new System.Drawing.Point(219, 74);
            this.cell6.Name = "cell6";
            this.cell6.Size = new System.Drawing.Size(20, 20);
            this.cell6.TabIndex = 5;
            this.cell6.TabStop = false;
            // 
            // cell3
            // 
            this.cell3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell3.Location = new System.Drawing.Point(117, 74);
            this.cell3.Name = "cell3";
            this.cell3.Size = new System.Drawing.Size(20, 20);
            this.cell3.TabIndex = 2;
            this.cell3.TabStop = false;
            // 
            // cell2
            // 
            this.cell2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell2.Location = new System.Drawing.Point(83, 74);
            this.cell2.Name = "cell2";
            this.cell2.Size = new System.Drawing.Size(20, 20);
            this.cell2.TabIndex = 1;
            this.cell2.TabStop = false;
            // 
            // cell32
            // 
            this.cell32.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell32.Location = new System.Drawing.Point(559, 110);
            this.cell32.Name = "cell32";
            this.cell32.Size = new System.Drawing.Size(20, 20);
            this.cell32.TabIndex = 0;
            this.cell32.TabStop = false;
            // 
            // cell31
            // 
            this.cell31.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell31.Location = new System.Drawing.Point(525, 110);
            this.cell31.Name = "cell31";
            this.cell31.Size = new System.Drawing.Size(20, 20);
            this.cell31.TabIndex = 0;
            this.cell31.TabStop = false;
            // 
            // cell30
            // 
            this.cell30.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell30.Location = new System.Drawing.Point(491, 110);
            this.cell30.Name = "cell30";
            this.cell30.Size = new System.Drawing.Size(20, 20);
            this.cell30.TabIndex = 0;
            this.cell30.TabStop = false;
            // 
            // cell29
            // 
            this.cell29.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell29.Location = new System.Drawing.Point(457, 110);
            this.cell29.Name = "cell29";
            this.cell29.Size = new System.Drawing.Size(20, 20);
            this.cell29.TabIndex = 0;
            this.cell29.TabStop = false;
            // 
            // cell28
            // 
            this.cell28.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell28.Location = new System.Drawing.Point(423, 110);
            this.cell28.Name = "cell28";
            this.cell28.Size = new System.Drawing.Size(20, 20);
            this.cell28.TabIndex = 0;
            this.cell28.TabStop = false;
            // 
            // cell27
            // 
            this.cell27.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell27.Location = new System.Drawing.Point(389, 110);
            this.cell27.Name = "cell27";
            this.cell27.Size = new System.Drawing.Size(20, 20);
            this.cell27.TabIndex = 0;
            this.cell27.TabStop = false;
            // 
            // cell26
            // 
            this.cell26.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell26.Location = new System.Drawing.Point(355, 110);
            this.cell26.Name = "cell26";
            this.cell26.Size = new System.Drawing.Size(20, 20);
            this.cell26.TabIndex = 0;
            this.cell26.TabStop = false;
            // 
            // cell25
            // 
            this.cell25.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell25.Location = new System.Drawing.Point(321, 110);
            this.cell25.Name = "cell25";
            this.cell25.Size = new System.Drawing.Size(20, 20);
            this.cell25.TabIndex = 0;
            this.cell25.TabStop = false;
            // 
            // cell24
            // 
            this.cell24.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell24.Location = new System.Drawing.Point(287, 110);
            this.cell24.Name = "cell24";
            this.cell24.Size = new System.Drawing.Size(20, 20);
            this.cell24.TabIndex = 23;
            this.cell24.TabStop = false;
            // 
            // cell23
            // 
            this.cell23.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell23.Location = new System.Drawing.Point(253, 110);
            this.cell23.Name = "cell23";
            this.cell23.Size = new System.Drawing.Size(20, 20);
            this.cell23.TabIndex = 22;
            this.cell23.TabStop = false;
            // 
            // cell22
            // 
            this.cell22.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell22.Location = new System.Drawing.Point(219, 110);
            this.cell22.Name = "cell22";
            this.cell22.Size = new System.Drawing.Size(20, 20);
            this.cell22.TabIndex = 21;
            this.cell22.TabStop = false;
            // 
            // cell21
            // 
            this.cell21.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell21.Location = new System.Drawing.Point(185, 110);
            this.cell21.Name = "cell21";
            this.cell21.Size = new System.Drawing.Size(20, 20);
            this.cell21.TabIndex = 20;
            this.cell21.TabStop = false;
            // 
            // cell20
            // 
            this.cell20.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell20.Location = new System.Drawing.Point(151, 110);
            this.cell20.Name = "cell20";
            this.cell20.Size = new System.Drawing.Size(20, 20);
            this.cell20.TabIndex = 19;
            this.cell20.TabStop = false;
            // 
            // cell19
            // 
            this.cell19.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell19.Location = new System.Drawing.Point(117, 110);
            this.cell19.Name = "cell19";
            this.cell19.Size = new System.Drawing.Size(20, 20);
            this.cell19.TabIndex = 18;
            this.cell19.TabStop = false;
            // 
            // cell18
            // 
            this.cell18.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell18.Location = new System.Drawing.Point(83, 110);
            this.cell18.Name = "cell18";
            this.cell18.Size = new System.Drawing.Size(20, 20);
            this.cell18.TabIndex = 17;
            this.cell18.TabStop = false;
            // 
            // cell48
            // 
            this.cell48.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell48.Location = new System.Drawing.Point(559, 146);
            this.cell48.Name = "cell48";
            this.cell48.Size = new System.Drawing.Size(20, 20);
            this.cell48.TabIndex = 0;
            this.cell48.TabStop = false;
            // 
            // cell47
            // 
            this.cell47.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell47.Location = new System.Drawing.Point(525, 146);
            this.cell47.Name = "cell47";
            this.cell47.Size = new System.Drawing.Size(20, 20);
            this.cell47.TabIndex = 0;
            this.cell47.TabStop = false;
            // 
            // cell46
            // 
            this.cell46.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell46.Location = new System.Drawing.Point(491, 146);
            this.cell46.Name = "cell46";
            this.cell46.Size = new System.Drawing.Size(20, 20);
            this.cell46.TabIndex = 0;
            this.cell46.TabStop = false;
            // 
            // cell45
            // 
            this.cell45.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell45.Location = new System.Drawing.Point(457, 146);
            this.cell45.Name = "cell45";
            this.cell45.Size = new System.Drawing.Size(20, 20);
            this.cell45.TabIndex = 0;
            this.cell45.TabStop = false;
            // 
            // cell44
            // 
            this.cell44.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell44.Location = new System.Drawing.Point(423, 146);
            this.cell44.Name = "cell44";
            this.cell44.Size = new System.Drawing.Size(20, 20);
            this.cell44.TabIndex = 0;
            this.cell44.TabStop = false;
            // 
            // cell43
            // 
            this.cell43.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell43.Location = new System.Drawing.Point(389, 146);
            this.cell43.Name = "cell43";
            this.cell43.Size = new System.Drawing.Size(20, 20);
            this.cell43.TabIndex = 0;
            this.cell43.TabStop = false;
            // 
            // cell42
            // 
            this.cell42.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell42.Location = new System.Drawing.Point(355, 146);
            this.cell42.Name = "cell42";
            this.cell42.Size = new System.Drawing.Size(20, 20);
            this.cell42.TabIndex = 0;
            this.cell42.TabStop = false;
            // 
            // cell41
            // 
            this.cell41.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell41.Location = new System.Drawing.Point(321, 146);
            this.cell41.Name = "cell41";
            this.cell41.Size = new System.Drawing.Size(20, 20);
            this.cell41.TabIndex = 0;
            this.cell41.TabStop = false;
            // 
            // cell40
            // 
            this.cell40.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell40.Location = new System.Drawing.Point(287, 146);
            this.cell40.Name = "cell40";
            this.cell40.Size = new System.Drawing.Size(20, 20);
            this.cell40.TabIndex = 0;
            this.cell40.TabStop = false;
            // 
            // cell39
            // 
            this.cell39.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell39.Location = new System.Drawing.Point(253, 146);
            this.cell39.Name = "cell39";
            this.cell39.Size = new System.Drawing.Size(20, 20);
            this.cell39.TabIndex = 0;
            this.cell39.TabStop = false;
            // 
            // cell38
            // 
            this.cell38.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell38.Location = new System.Drawing.Point(219, 146);
            this.cell38.Name = "cell38";
            this.cell38.Size = new System.Drawing.Size(20, 20);
            this.cell38.TabIndex = 0;
            this.cell38.TabStop = false;
            // 
            // cell37
            // 
            this.cell37.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell37.Location = new System.Drawing.Point(185, 146);
            this.cell37.Name = "cell37";
            this.cell37.Size = new System.Drawing.Size(20, 20);
            this.cell37.TabIndex = 0;
            this.cell37.TabStop = false;
            // 
            // cell36
            // 
            this.cell36.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell36.Location = new System.Drawing.Point(151, 146);
            this.cell36.Name = "cell36";
            this.cell36.Size = new System.Drawing.Size(20, 20);
            this.cell36.TabIndex = 0;
            this.cell36.TabStop = false;
            // 
            // cell35
            // 
            this.cell35.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell35.Location = new System.Drawing.Point(117, 146);
            this.cell35.Name = "cell35";
            this.cell35.Size = new System.Drawing.Size(20, 20);
            this.cell35.TabIndex = 0;
            this.cell35.TabStop = false;
            // 
            // cell34
            // 
            this.cell34.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell34.Location = new System.Drawing.Point(83, 146);
            this.cell34.Name = "cell34";
            this.cell34.Size = new System.Drawing.Size(20, 20);
            this.cell34.TabIndex = 0;
            this.cell34.TabStop = false;
            // 
            // cell64
            // 
            this.cell64.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell64.Location = new System.Drawing.Point(559, 182);
            this.cell64.Name = "cell64";
            this.cell64.Size = new System.Drawing.Size(20, 20);
            this.cell64.TabIndex = 0;
            this.cell64.TabStop = false;
            // 
            // cell63
            // 
            this.cell63.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell63.Location = new System.Drawing.Point(525, 182);
            this.cell63.Name = "cell63";
            this.cell63.Size = new System.Drawing.Size(20, 20);
            this.cell63.TabIndex = 0;
            this.cell63.TabStop = false;
            // 
            // cell62
            // 
            this.cell62.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell62.Location = new System.Drawing.Point(491, 182);
            this.cell62.Name = "cell62";
            this.cell62.Size = new System.Drawing.Size(20, 20);
            this.cell62.TabIndex = 0;
            this.cell62.TabStop = false;
            // 
            // cell61
            // 
            this.cell61.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell61.Location = new System.Drawing.Point(457, 182);
            this.cell61.Name = "cell61";
            this.cell61.Size = new System.Drawing.Size(20, 20);
            this.cell61.TabIndex = 0;
            this.cell61.TabStop = false;
            // 
            // cell60
            // 
            this.cell60.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell60.Location = new System.Drawing.Point(423, 182);
            this.cell60.Name = "cell60";
            this.cell60.Size = new System.Drawing.Size(20, 20);
            this.cell60.TabIndex = 0;
            this.cell60.TabStop = false;
            // 
            // cell59
            // 
            this.cell59.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell59.Location = new System.Drawing.Point(389, 182);
            this.cell59.Name = "cell59";
            this.cell59.Size = new System.Drawing.Size(20, 20);
            this.cell59.TabIndex = 0;
            this.cell59.TabStop = false;
            // 
            // cell58
            // 
            this.cell58.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell58.Location = new System.Drawing.Point(355, 182);
            this.cell58.Name = "cell58";
            this.cell58.Size = new System.Drawing.Size(20, 20);
            this.cell58.TabIndex = 0;
            this.cell58.TabStop = false;
            // 
            // cell57
            // 
            this.cell57.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell57.Location = new System.Drawing.Point(321, 182);
            this.cell57.Name = "cell57";
            this.cell57.Size = new System.Drawing.Size(20, 20);
            this.cell57.TabIndex = 0;
            this.cell57.TabStop = false;
            // 
            // cell56
            // 
            this.cell56.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell56.Location = new System.Drawing.Point(287, 182);
            this.cell56.Name = "cell56";
            this.cell56.Size = new System.Drawing.Size(20, 20);
            this.cell56.TabIndex = 0;
            this.cell56.TabStop = false;
            // 
            // cell55
            // 
            this.cell55.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell55.Location = new System.Drawing.Point(253, 182);
            this.cell55.Name = "cell55";
            this.cell55.Size = new System.Drawing.Size(20, 20);
            this.cell55.TabIndex = 0;
            this.cell55.TabStop = false;
            // 
            // cell54
            // 
            this.cell54.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell54.Location = new System.Drawing.Point(219, 182);
            this.cell54.Name = "cell54";
            this.cell54.Size = new System.Drawing.Size(20, 20);
            this.cell54.TabIndex = 0;
            this.cell54.TabStop = false;
            // 
            // cell53
            // 
            this.cell53.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell53.Location = new System.Drawing.Point(185, 182);
            this.cell53.Name = "cell53";
            this.cell53.Size = new System.Drawing.Size(20, 20);
            this.cell53.TabIndex = 0;
            this.cell53.TabStop = false;
            // 
            // cell52
            // 
            this.cell52.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell52.Location = new System.Drawing.Point(151, 182);
            this.cell52.Name = "cell52";
            this.cell52.Size = new System.Drawing.Size(20, 20);
            this.cell52.TabIndex = 0;
            this.cell52.TabStop = false;
            // 
            // cell51
            // 
            this.cell51.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell51.Location = new System.Drawing.Point(117, 182);
            this.cell51.Name = "cell51";
            this.cell51.Size = new System.Drawing.Size(20, 20);
            this.cell51.TabIndex = 0;
            this.cell51.TabStop = false;
            // 
            // cell50
            // 
            this.cell50.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell50.Location = new System.Drawing.Point(83, 182);
            this.cell50.Name = "cell50";
            this.cell50.Size = new System.Drawing.Size(20, 20);
            this.cell50.TabIndex = 0;
            this.cell50.TabStop = false;
            // 
            // cell80
            // 
            this.cell80.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell80.Location = new System.Drawing.Point(559, 218);
            this.cell80.Name = "cell80";
            this.cell80.Size = new System.Drawing.Size(20, 20);
            this.cell80.TabIndex = 0;
            this.cell80.TabStop = false;
            // 
            // cell79
            // 
            this.cell79.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell79.Location = new System.Drawing.Point(525, 218);
            this.cell79.Name = "cell79";
            this.cell79.Size = new System.Drawing.Size(20, 20);
            this.cell79.TabIndex = 0;
            this.cell79.TabStop = false;
            // 
            // cell78
            // 
            this.cell78.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell78.Location = new System.Drawing.Point(491, 218);
            this.cell78.Name = "cell78";
            this.cell78.Size = new System.Drawing.Size(20, 20);
            this.cell78.TabIndex = 0;
            this.cell78.TabStop = false;
            // 
            // cell77
            // 
            this.cell77.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell77.Location = new System.Drawing.Point(457, 218);
            this.cell77.Name = "cell77";
            this.cell77.Size = new System.Drawing.Size(20, 20);
            this.cell77.TabIndex = 0;
            this.cell77.TabStop = false;
            // 
            // cell76
            // 
            this.cell76.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell76.Location = new System.Drawing.Point(423, 218);
            this.cell76.Name = "cell76";
            this.cell76.Size = new System.Drawing.Size(20, 20);
            this.cell76.TabIndex = 0;
            this.cell76.TabStop = false;
            // 
            // cell75
            // 
            this.cell75.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell75.Location = new System.Drawing.Point(389, 218);
            this.cell75.Name = "cell75";
            this.cell75.Size = new System.Drawing.Size(20, 20);
            this.cell75.TabIndex = 0;
            this.cell75.TabStop = false;
            // 
            // cell74
            // 
            this.cell74.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell74.Location = new System.Drawing.Point(355, 218);
            this.cell74.Name = "cell74";
            this.cell74.Size = new System.Drawing.Size(20, 20);
            this.cell74.TabIndex = 0;
            this.cell74.TabStop = false;
            // 
            // cell73
            // 
            this.cell73.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell73.Location = new System.Drawing.Point(321, 218);
            this.cell73.Name = "cell73";
            this.cell73.Size = new System.Drawing.Size(20, 20);
            this.cell73.TabIndex = 0;
            this.cell73.TabStop = false;
            // 
            // cell72
            // 
            this.cell72.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell72.Location = new System.Drawing.Point(287, 218);
            this.cell72.Name = "cell72";
            this.cell72.Size = new System.Drawing.Size(20, 20);
            this.cell72.TabIndex = 0;
            this.cell72.TabStop = false;
            // 
            // cell71
            // 
            this.cell71.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell71.Location = new System.Drawing.Point(253, 218);
            this.cell71.Name = "cell71";
            this.cell71.Size = new System.Drawing.Size(20, 20);
            this.cell71.TabIndex = 0;
            this.cell71.TabStop = false;
            // 
            // cell70
            // 
            this.cell70.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell70.Location = new System.Drawing.Point(219, 218);
            this.cell70.Name = "cell70";
            this.cell70.Size = new System.Drawing.Size(20, 20);
            this.cell70.TabIndex = 0;
            this.cell70.TabStop = false;
            // 
            // cell69
            // 
            this.cell69.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell69.Location = new System.Drawing.Point(185, 218);
            this.cell69.Name = "cell69";
            this.cell69.Size = new System.Drawing.Size(20, 20);
            this.cell69.TabIndex = 0;
            this.cell69.TabStop = false;
            // 
            // cell68
            // 
            this.cell68.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell68.Location = new System.Drawing.Point(151, 218);
            this.cell68.Name = "cell68";
            this.cell68.Size = new System.Drawing.Size(20, 20);
            this.cell68.TabIndex = 0;
            this.cell68.TabStop = false;
            // 
            // cell67
            // 
            this.cell67.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell67.Location = new System.Drawing.Point(117, 218);
            this.cell67.Name = "cell67";
            this.cell67.Size = new System.Drawing.Size(20, 20);
            this.cell67.TabIndex = 0;
            this.cell67.TabStop = false;
            // 
            // cell66
            // 
            this.cell66.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell66.Location = new System.Drawing.Point(83, 218);
            this.cell66.Name = "cell66";
            this.cell66.Size = new System.Drawing.Size(20, 20);
            this.cell66.TabIndex = 0;
            this.cell66.TabStop = false;
            // 
            // cell96
            // 
            this.cell96.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell96.Location = new System.Drawing.Point(559, 254);
            this.cell96.Name = "cell96";
            this.cell96.Size = new System.Drawing.Size(20, 20);
            this.cell96.TabIndex = 0;
            this.cell96.TabStop = false;
            // 
            // cell95
            // 
            this.cell95.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell95.Location = new System.Drawing.Point(525, 254);
            this.cell95.Name = "cell95";
            this.cell95.Size = new System.Drawing.Size(20, 20);
            this.cell95.TabIndex = 0;
            this.cell95.TabStop = false;
            // 
            // cell94
            // 
            this.cell94.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell94.Location = new System.Drawing.Point(491, 254);
            this.cell94.Name = "cell94";
            this.cell94.Size = new System.Drawing.Size(20, 20);
            this.cell94.TabIndex = 0;
            this.cell94.TabStop = false;
            // 
            // cell93
            // 
            this.cell93.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell93.Location = new System.Drawing.Point(457, 254);
            this.cell93.Name = "cell93";
            this.cell93.Size = new System.Drawing.Size(20, 20);
            this.cell93.TabIndex = 0;
            this.cell93.TabStop = false;
            // 
            // cell92
            // 
            this.cell92.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell92.Location = new System.Drawing.Point(423, 254);
            this.cell92.Name = "cell92";
            this.cell92.Size = new System.Drawing.Size(20, 20);
            this.cell92.TabIndex = 0;
            this.cell92.TabStop = false;
            // 
            // cell91
            // 
            this.cell91.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell91.Location = new System.Drawing.Point(389, 254);
            this.cell91.Name = "cell91";
            this.cell91.Size = new System.Drawing.Size(20, 20);
            this.cell91.TabIndex = 0;
            this.cell91.TabStop = false;
            // 
            // cell90
            // 
            this.cell90.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell90.Location = new System.Drawing.Point(355, 254);
            this.cell90.Name = "cell90";
            this.cell90.Size = new System.Drawing.Size(20, 20);
            this.cell90.TabIndex = 0;
            this.cell90.TabStop = false;
            // 
            // cell89
            // 
            this.cell89.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell89.Location = new System.Drawing.Point(321, 254);
            this.cell89.Name = "cell89";
            this.cell89.Size = new System.Drawing.Size(20, 20);
            this.cell89.TabIndex = 0;
            this.cell89.TabStop = false;
            // 
            // cell88
            // 
            this.cell88.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell88.Location = new System.Drawing.Point(287, 254);
            this.cell88.Name = "cell88";
            this.cell88.Size = new System.Drawing.Size(20, 20);
            this.cell88.TabIndex = 0;
            this.cell88.TabStop = false;
            // 
            // cell87
            // 
            this.cell87.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell87.Location = new System.Drawing.Point(253, 254);
            this.cell87.Name = "cell87";
            this.cell87.Size = new System.Drawing.Size(20, 20);
            this.cell87.TabIndex = 0;
            this.cell87.TabStop = false;
            // 
            // cell86
            // 
            this.cell86.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell86.Location = new System.Drawing.Point(219, 254);
            this.cell86.Name = "cell86";
            this.cell86.Size = new System.Drawing.Size(20, 20);
            this.cell86.TabIndex = 0;
            this.cell86.TabStop = false;
            // 
            // cell85
            // 
            this.cell85.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell85.Location = new System.Drawing.Point(185, 254);
            this.cell85.Name = "cell85";
            this.cell85.Size = new System.Drawing.Size(20, 20);
            this.cell85.TabIndex = 0;
            this.cell85.TabStop = false;
            // 
            // cell84
            // 
            this.cell84.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell84.Location = new System.Drawing.Point(151, 254);
            this.cell84.Name = "cell84";
            this.cell84.Size = new System.Drawing.Size(20, 20);
            this.cell84.TabIndex = 0;
            this.cell84.TabStop = false;
            // 
            // cell83
            // 
            this.cell83.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell83.Location = new System.Drawing.Point(117, 254);
            this.cell83.Name = "cell83";
            this.cell83.Size = new System.Drawing.Size(20, 20);
            this.cell83.TabIndex = 0;
            this.cell83.TabStop = false;
            // 
            // cell82
            // 
            this.cell82.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell82.Location = new System.Drawing.Point(83, 254);
            this.cell82.Name = "cell82";
            this.cell82.Size = new System.Drawing.Size(20, 20);
            this.cell82.TabIndex = 0;
            this.cell82.TabStop = false;
            // 
            // cell112
            // 
            this.cell112.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell112.Location = new System.Drawing.Point(559, 290);
            this.cell112.Name = "cell112";
            this.cell112.Size = new System.Drawing.Size(20, 20);
            this.cell112.TabIndex = 0;
            this.cell112.TabStop = false;
            // 
            // cell111
            // 
            this.cell111.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell111.Location = new System.Drawing.Point(525, 290);
            this.cell111.Name = "cell111";
            this.cell111.Size = new System.Drawing.Size(20, 20);
            this.cell111.TabIndex = 0;
            this.cell111.TabStop = false;
            // 
            // cell110
            // 
            this.cell110.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell110.Location = new System.Drawing.Point(491, 290);
            this.cell110.Name = "cell110";
            this.cell110.Size = new System.Drawing.Size(20, 20);
            this.cell110.TabIndex = 0;
            this.cell110.TabStop = false;
            // 
            // cell109
            // 
            this.cell109.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell109.Location = new System.Drawing.Point(457, 290);
            this.cell109.Name = "cell109";
            this.cell109.Size = new System.Drawing.Size(20, 20);
            this.cell109.TabIndex = 0;
            this.cell109.TabStop = false;
            // 
            // cell108
            // 
            this.cell108.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell108.Location = new System.Drawing.Point(423, 290);
            this.cell108.Name = "cell108";
            this.cell108.Size = new System.Drawing.Size(20, 20);
            this.cell108.TabIndex = 0;
            this.cell108.TabStop = false;
            // 
            // cell107
            // 
            this.cell107.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell107.Location = new System.Drawing.Point(389, 290);
            this.cell107.Name = "cell107";
            this.cell107.Size = new System.Drawing.Size(20, 20);
            this.cell107.TabIndex = 0;
            this.cell107.TabStop = false;
            // 
            // cell106
            // 
            this.cell106.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell106.Location = new System.Drawing.Point(355, 290);
            this.cell106.Name = "cell106";
            this.cell106.Size = new System.Drawing.Size(20, 20);
            this.cell106.TabIndex = 0;
            this.cell106.TabStop = false;
            // 
            // cell105
            // 
            this.cell105.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell105.Location = new System.Drawing.Point(321, 290);
            this.cell105.Name = "cell105";
            this.cell105.Size = new System.Drawing.Size(20, 20);
            this.cell105.TabIndex = 0;
            this.cell105.TabStop = false;
            // 
            // cell104
            // 
            this.cell104.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell104.Location = new System.Drawing.Point(287, 290);
            this.cell104.Name = "cell104";
            this.cell104.Size = new System.Drawing.Size(20, 20);
            this.cell104.TabIndex = 0;
            this.cell104.TabStop = false;
            // 
            // cell103
            // 
            this.cell103.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell103.Location = new System.Drawing.Point(253, 290);
            this.cell103.Name = "cell103";
            this.cell103.Size = new System.Drawing.Size(20, 20);
            this.cell103.TabIndex = 0;
            this.cell103.TabStop = false;
            // 
            // cell102
            // 
            this.cell102.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell102.Location = new System.Drawing.Point(219, 290);
            this.cell102.Name = "cell102";
            this.cell102.Size = new System.Drawing.Size(20, 20);
            this.cell102.TabIndex = 0;
            this.cell102.TabStop = false;
            // 
            // cell101
            // 
            this.cell101.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell101.Location = new System.Drawing.Point(185, 290);
            this.cell101.Name = "cell101";
            this.cell101.Size = new System.Drawing.Size(20, 20);
            this.cell101.TabIndex = 0;
            this.cell101.TabStop = false;
            // 
            // cell100
            // 
            this.cell100.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell100.Location = new System.Drawing.Point(151, 290);
            this.cell100.Name = "cell100";
            this.cell100.Size = new System.Drawing.Size(20, 20);
            this.cell100.TabIndex = 0;
            this.cell100.TabStop = false;
            // 
            // cell99
            // 
            this.cell99.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell99.Location = new System.Drawing.Point(117, 290);
            this.cell99.Name = "cell99";
            this.cell99.Size = new System.Drawing.Size(20, 20);
            this.cell99.TabIndex = 0;
            this.cell99.TabStop = false;
            // 
            // cell98
            // 
            this.cell98.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell98.Location = new System.Drawing.Point(83, 290);
            this.cell98.Name = "cell98";
            this.cell98.Size = new System.Drawing.Size(20, 20);
            this.cell98.TabIndex = 0;
            this.cell98.TabStop = false;
            // 
            // cell128
            // 
            this.cell128.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell128.Location = new System.Drawing.Point(559, 326);
            this.cell128.Name = "cell128";
            this.cell128.Size = new System.Drawing.Size(20, 20);
            this.cell128.TabIndex = 0;
            this.cell128.TabStop = false;
            // 
            // cell127
            // 
            this.cell127.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell127.Location = new System.Drawing.Point(525, 326);
            this.cell127.Name = "cell127";
            this.cell127.Size = new System.Drawing.Size(20, 20);
            this.cell127.TabIndex = 0;
            this.cell127.TabStop = false;
            // 
            // cell126
            // 
            this.cell126.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell126.Location = new System.Drawing.Point(491, 326);
            this.cell126.Name = "cell126";
            this.cell126.Size = new System.Drawing.Size(20, 20);
            this.cell126.TabIndex = 0;
            this.cell126.TabStop = false;
            // 
            // cell125
            // 
            this.cell125.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell125.Location = new System.Drawing.Point(457, 326);
            this.cell125.Name = "cell125";
            this.cell125.Size = new System.Drawing.Size(20, 20);
            this.cell125.TabIndex = 0;
            this.cell125.TabStop = false;
            // 
            // cell124
            // 
            this.cell124.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell124.Location = new System.Drawing.Point(423, 326);
            this.cell124.Name = "cell124";
            this.cell124.Size = new System.Drawing.Size(20, 20);
            this.cell124.TabIndex = 0;
            this.cell124.TabStop = false;
            // 
            // cell123
            // 
            this.cell123.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell123.Location = new System.Drawing.Point(389, 326);
            this.cell123.Name = "cell123";
            this.cell123.Size = new System.Drawing.Size(20, 20);
            this.cell123.TabIndex = 0;
            this.cell123.TabStop = false;
            // 
            // cell122
            // 
            this.cell122.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell122.Location = new System.Drawing.Point(355, 326);
            this.cell122.Name = "cell122";
            this.cell122.Size = new System.Drawing.Size(20, 20);
            this.cell122.TabIndex = 0;
            this.cell122.TabStop = false;
            // 
            // cell121
            // 
            this.cell121.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell121.Location = new System.Drawing.Point(321, 326);
            this.cell121.Name = "cell121";
            this.cell121.Size = new System.Drawing.Size(20, 20);
            this.cell121.TabIndex = 0;
            this.cell121.TabStop = false;
            // 
            // cell120
            // 
            this.cell120.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell120.Location = new System.Drawing.Point(287, 326);
            this.cell120.Name = "cell120";
            this.cell120.Size = new System.Drawing.Size(20, 20);
            this.cell120.TabIndex = 0;
            this.cell120.TabStop = false;
            // 
            // cell119
            // 
            this.cell119.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell119.Location = new System.Drawing.Point(253, 326);
            this.cell119.Name = "cell119";
            this.cell119.Size = new System.Drawing.Size(20, 20);
            this.cell119.TabIndex = 0;
            this.cell119.TabStop = false;
            // 
            // cell118
            // 
            this.cell118.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell118.Location = new System.Drawing.Point(219, 326);
            this.cell118.Name = "cell118";
            this.cell118.Size = new System.Drawing.Size(20, 20);
            this.cell118.TabIndex = 0;
            this.cell118.TabStop = false;
            // 
            // cell117
            // 
            this.cell117.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell117.Location = new System.Drawing.Point(185, 326);
            this.cell117.Name = "cell117";
            this.cell117.Size = new System.Drawing.Size(20, 20);
            this.cell117.TabIndex = 0;
            this.cell117.TabStop = false;
            // 
            // cell116
            // 
            this.cell116.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell116.Location = new System.Drawing.Point(151, 326);
            this.cell116.Name = "cell116";
            this.cell116.Size = new System.Drawing.Size(20, 20);
            this.cell116.TabIndex = 0;
            this.cell116.TabStop = false;
            // 
            // cell115
            // 
            this.cell115.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell115.Location = new System.Drawing.Point(117, 326);
            this.cell115.Name = "cell115";
            this.cell115.Size = new System.Drawing.Size(20, 20);
            this.cell115.TabIndex = 0;
            this.cell115.TabStop = false;
            // 
            // cell114
            // 
            this.cell114.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell114.Location = new System.Drawing.Point(83, 326);
            this.cell114.Name = "cell114";
            this.cell114.Size = new System.Drawing.Size(20, 20);
            this.cell114.TabIndex = 0;
            this.cell114.TabStop = false;
            // 
            // cell144
            // 
            this.cell144.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell144.Location = new System.Drawing.Point(559, 362);
            this.cell144.Name = "cell144";
            this.cell144.Size = new System.Drawing.Size(20, 20);
            this.cell144.TabIndex = 0;
            this.cell144.TabStop = false;
            // 
            // cell143
            // 
            this.cell143.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell143.Location = new System.Drawing.Point(525, 362);
            this.cell143.Name = "cell143";
            this.cell143.Size = new System.Drawing.Size(20, 20);
            this.cell143.TabIndex = 0;
            this.cell143.TabStop = false;
            // 
            // cell142
            // 
            this.cell142.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell142.Location = new System.Drawing.Point(491, 362);
            this.cell142.Name = "cell142";
            this.cell142.Size = new System.Drawing.Size(20, 20);
            this.cell142.TabIndex = 0;
            this.cell142.TabStop = false;
            // 
            // cell141
            // 
            this.cell141.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell141.Location = new System.Drawing.Point(457, 362);
            this.cell141.Name = "cell141";
            this.cell141.Size = new System.Drawing.Size(20, 20);
            this.cell141.TabIndex = 0;
            this.cell141.TabStop = false;
            // 
            // cell140
            // 
            this.cell140.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell140.Location = new System.Drawing.Point(423, 362);
            this.cell140.Name = "cell140";
            this.cell140.Size = new System.Drawing.Size(20, 20);
            this.cell140.TabIndex = 0;
            this.cell140.TabStop = false;
            // 
            // cell139
            // 
            this.cell139.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell139.Location = new System.Drawing.Point(389, 362);
            this.cell139.Name = "cell139";
            this.cell139.Size = new System.Drawing.Size(20, 20);
            this.cell139.TabIndex = 0;
            this.cell139.TabStop = false;
            // 
            // cell138
            // 
            this.cell138.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell138.Location = new System.Drawing.Point(355, 362);
            this.cell138.Name = "cell138";
            this.cell138.Size = new System.Drawing.Size(20, 20);
            this.cell138.TabIndex = 0;
            this.cell138.TabStop = false;
            // 
            // cell137
            // 
            this.cell137.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell137.Location = new System.Drawing.Point(321, 362);
            this.cell137.Name = "cell137";
            this.cell137.Size = new System.Drawing.Size(20, 20);
            this.cell137.TabIndex = 0;
            this.cell137.TabStop = false;
            // 
            // cell136
            // 
            this.cell136.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell136.Location = new System.Drawing.Point(287, 362);
            this.cell136.Name = "cell136";
            this.cell136.Size = new System.Drawing.Size(20, 20);
            this.cell136.TabIndex = 0;
            this.cell136.TabStop = false;
            // 
            // cell135
            // 
            this.cell135.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell135.Location = new System.Drawing.Point(253, 362);
            this.cell135.Name = "cell135";
            this.cell135.Size = new System.Drawing.Size(20, 20);
            this.cell135.TabIndex = 0;
            this.cell135.TabStop = false;
            // 
            // cell134
            // 
            this.cell134.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell134.Location = new System.Drawing.Point(219, 362);
            this.cell134.Name = "cell134";
            this.cell134.Size = new System.Drawing.Size(20, 20);
            this.cell134.TabIndex = 0;
            this.cell134.TabStop = false;
            // 
            // cell133
            // 
            this.cell133.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell133.Location = new System.Drawing.Point(185, 362);
            this.cell133.Name = "cell133";
            this.cell133.Size = new System.Drawing.Size(20, 20);
            this.cell133.TabIndex = 0;
            this.cell133.TabStop = false;
            // 
            // cell132
            // 
            this.cell132.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell132.Location = new System.Drawing.Point(151, 362);
            this.cell132.Name = "cell132";
            this.cell132.Size = new System.Drawing.Size(20, 20);
            this.cell132.TabIndex = 0;
            this.cell132.TabStop = false;
            // 
            // cell131
            // 
            this.cell131.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell131.Location = new System.Drawing.Point(117, 362);
            this.cell131.Name = "cell131";
            this.cell131.Size = new System.Drawing.Size(20, 20);
            this.cell131.TabIndex = 0;
            this.cell131.TabStop = false;
            // 
            // cell130
            // 
            this.cell130.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell130.Location = new System.Drawing.Point(83, 362);
            this.cell130.Name = "cell130";
            this.cell130.Size = new System.Drawing.Size(20, 20);
            this.cell130.TabIndex = 0;
            this.cell130.TabStop = false;
            // 
            // cell160
            // 
            this.cell160.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell160.Location = new System.Drawing.Point(559, 398);
            this.cell160.Name = "cell160";
            this.cell160.Size = new System.Drawing.Size(20, 20);
            this.cell160.TabIndex = 0;
            this.cell160.TabStop = false;
            // 
            // cell159
            // 
            this.cell159.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell159.Location = new System.Drawing.Point(525, 398);
            this.cell159.Name = "cell159";
            this.cell159.Size = new System.Drawing.Size(20, 20);
            this.cell159.TabIndex = 0;
            this.cell159.TabStop = false;
            // 
            // cell158
            // 
            this.cell158.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell158.Location = new System.Drawing.Point(491, 398);
            this.cell158.Name = "cell158";
            this.cell158.Size = new System.Drawing.Size(20, 20);
            this.cell158.TabIndex = 0;
            this.cell158.TabStop = false;
            // 
            // cell157
            // 
            this.cell157.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell157.Location = new System.Drawing.Point(457, 398);
            this.cell157.Name = "cell157";
            this.cell157.Size = new System.Drawing.Size(20, 20);
            this.cell157.TabIndex = 0;
            this.cell157.TabStop = false;
            // 
            // cell156
            // 
            this.cell156.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell156.Location = new System.Drawing.Point(423, 398);
            this.cell156.Name = "cell156";
            this.cell156.Size = new System.Drawing.Size(20, 20);
            this.cell156.TabIndex = 0;
            this.cell156.TabStop = false;
            // 
            // cell155
            // 
            this.cell155.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell155.Location = new System.Drawing.Point(389, 398);
            this.cell155.Name = "cell155";
            this.cell155.Size = new System.Drawing.Size(20, 20);
            this.cell155.TabIndex = 0;
            this.cell155.TabStop = false;
            // 
            // cell154
            // 
            this.cell154.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell154.Location = new System.Drawing.Point(355, 398);
            this.cell154.Name = "cell154";
            this.cell154.Size = new System.Drawing.Size(20, 20);
            this.cell154.TabIndex = 0;
            this.cell154.TabStop = false;
            // 
            // cell153
            // 
            this.cell153.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell153.Location = new System.Drawing.Point(321, 398);
            this.cell153.Name = "cell153";
            this.cell153.Size = new System.Drawing.Size(20, 20);
            this.cell153.TabIndex = 0;
            this.cell153.TabStop = false;
            // 
            // cell152
            // 
            this.cell152.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell152.Location = new System.Drawing.Point(287, 398);
            this.cell152.Name = "cell152";
            this.cell152.Size = new System.Drawing.Size(20, 20);
            this.cell152.TabIndex = 0;
            this.cell152.TabStop = false;
            // 
            // cell151
            // 
            this.cell151.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell151.Location = new System.Drawing.Point(253, 398);
            this.cell151.Name = "cell151";
            this.cell151.Size = new System.Drawing.Size(20, 20);
            this.cell151.TabIndex = 0;
            this.cell151.TabStop = false;
            // 
            // cell150
            // 
            this.cell150.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell150.Location = new System.Drawing.Point(219, 398);
            this.cell150.Name = "cell150";
            this.cell150.Size = new System.Drawing.Size(20, 20);
            this.cell150.TabIndex = 0;
            this.cell150.TabStop = false;
            // 
            // cell149
            // 
            this.cell149.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell149.Location = new System.Drawing.Point(185, 398);
            this.cell149.Name = "cell149";
            this.cell149.Size = new System.Drawing.Size(20, 20);
            this.cell149.TabIndex = 0;
            this.cell149.TabStop = false;
            // 
            // cell148
            // 
            this.cell148.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell148.Location = new System.Drawing.Point(151, 398);
            this.cell148.Name = "cell148";
            this.cell148.Size = new System.Drawing.Size(20, 20);
            this.cell148.TabIndex = 0;
            this.cell148.TabStop = false;
            // 
            // cell147
            // 
            this.cell147.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell147.Location = new System.Drawing.Point(117, 398);
            this.cell147.Name = "cell147";
            this.cell147.Size = new System.Drawing.Size(20, 20);
            this.cell147.TabIndex = 0;
            this.cell147.TabStop = false;
            // 
            // cell146
            // 
            this.cell146.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell146.Location = new System.Drawing.Point(83, 398);
            this.cell146.Name = "cell146";
            this.cell146.Size = new System.Drawing.Size(20, 20);
            this.cell146.TabIndex = 0;
            this.cell146.TabStop = false;
            // 
            // cell176
            // 
            this.cell176.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell176.Location = new System.Drawing.Point(559, 434);
            this.cell176.Name = "cell176";
            this.cell176.Size = new System.Drawing.Size(20, 20);
            this.cell176.TabIndex = 0;
            this.cell176.TabStop = false;
            // 
            // cell175
            // 
            this.cell175.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell175.Location = new System.Drawing.Point(525, 434);
            this.cell175.Name = "cell175";
            this.cell175.Size = new System.Drawing.Size(20, 20);
            this.cell175.TabIndex = 0;
            this.cell175.TabStop = false;
            // 
            // cell174
            // 
            this.cell174.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell174.Location = new System.Drawing.Point(491, 434);
            this.cell174.Name = "cell174";
            this.cell174.Size = new System.Drawing.Size(20, 20);
            this.cell174.TabIndex = 0;
            this.cell174.TabStop = false;
            // 
            // cell173
            // 
            this.cell173.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell173.Location = new System.Drawing.Point(457, 434);
            this.cell173.Name = "cell173";
            this.cell173.Size = new System.Drawing.Size(20, 20);
            this.cell173.TabIndex = 0;
            this.cell173.TabStop = false;
            // 
            // cell172
            // 
            this.cell172.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell172.Location = new System.Drawing.Point(423, 434);
            this.cell172.Name = "cell172";
            this.cell172.Size = new System.Drawing.Size(20, 20);
            this.cell172.TabIndex = 0;
            this.cell172.TabStop = false;
            // 
            // cell171
            // 
            this.cell171.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell171.Location = new System.Drawing.Point(389, 434);
            this.cell171.Name = "cell171";
            this.cell171.Size = new System.Drawing.Size(20, 20);
            this.cell171.TabIndex = 0;
            this.cell171.TabStop = false;
            // 
            // cell170
            // 
            this.cell170.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell170.Location = new System.Drawing.Point(355, 434);
            this.cell170.Name = "cell170";
            this.cell170.Size = new System.Drawing.Size(20, 20);
            this.cell170.TabIndex = 0;
            this.cell170.TabStop = false;
            // 
            // cell169
            // 
            this.cell169.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell169.Location = new System.Drawing.Point(321, 434);
            this.cell169.Name = "cell169";
            this.cell169.Size = new System.Drawing.Size(20, 20);
            this.cell169.TabIndex = 0;
            this.cell169.TabStop = false;
            // 
            // cell168
            // 
            this.cell168.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell168.Location = new System.Drawing.Point(287, 434);
            this.cell168.Name = "cell168";
            this.cell168.Size = new System.Drawing.Size(20, 20);
            this.cell168.TabIndex = 0;
            this.cell168.TabStop = false;
            // 
            // cell167
            // 
            this.cell167.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell167.Location = new System.Drawing.Point(253, 434);
            this.cell167.Name = "cell167";
            this.cell167.Size = new System.Drawing.Size(20, 20);
            this.cell167.TabIndex = 0;
            this.cell167.TabStop = false;
            // 
            // cell166
            // 
            this.cell166.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell166.Location = new System.Drawing.Point(219, 434);
            this.cell166.Name = "cell166";
            this.cell166.Size = new System.Drawing.Size(20, 20);
            this.cell166.TabIndex = 0;
            this.cell166.TabStop = false;
            // 
            // cell165
            // 
            this.cell165.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell165.Location = new System.Drawing.Point(185, 434);
            this.cell165.Name = "cell165";
            this.cell165.Size = new System.Drawing.Size(20, 20);
            this.cell165.TabIndex = 0;
            this.cell165.TabStop = false;
            // 
            // cell164
            // 
            this.cell164.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell164.Location = new System.Drawing.Point(151, 434);
            this.cell164.Name = "cell164";
            this.cell164.Size = new System.Drawing.Size(20, 20);
            this.cell164.TabIndex = 0;
            this.cell164.TabStop = false;
            // 
            // cell163
            // 
            this.cell163.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell163.Location = new System.Drawing.Point(117, 434);
            this.cell163.Name = "cell163";
            this.cell163.Size = new System.Drawing.Size(20, 20);
            this.cell163.TabIndex = 0;
            this.cell163.TabStop = false;
            // 
            // cell162
            // 
            this.cell162.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell162.Location = new System.Drawing.Point(83, 434);
            this.cell162.Name = "cell162";
            this.cell162.Size = new System.Drawing.Size(20, 20);
            this.cell162.TabIndex = 0;
            this.cell162.TabStop = false;
            // 
            // cell192
            // 
            this.cell192.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell192.Location = new System.Drawing.Point(559, 471);
            this.cell192.Name = "cell192";
            this.cell192.Size = new System.Drawing.Size(20, 20);
            this.cell192.TabIndex = 0;
            this.cell192.TabStop = false;
            // 
            // cell191
            // 
            this.cell191.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell191.Location = new System.Drawing.Point(525, 471);
            this.cell191.Name = "cell191";
            this.cell191.Size = new System.Drawing.Size(20, 20);
            this.cell191.TabIndex = 0;
            this.cell191.TabStop = false;
            // 
            // cell190
            // 
            this.cell190.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell190.Location = new System.Drawing.Point(491, 471);
            this.cell190.Name = "cell190";
            this.cell190.Size = new System.Drawing.Size(20, 20);
            this.cell190.TabIndex = 0;
            this.cell190.TabStop = false;
            // 
            // cell189
            // 
            this.cell189.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell189.Location = new System.Drawing.Point(457, 471);
            this.cell189.Name = "cell189";
            this.cell189.Size = new System.Drawing.Size(20, 20);
            this.cell189.TabIndex = 0;
            this.cell189.TabStop = false;
            // 
            // cell188
            // 
            this.cell188.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell188.Location = new System.Drawing.Point(423, 471);
            this.cell188.Name = "cell188";
            this.cell188.Size = new System.Drawing.Size(20, 20);
            this.cell188.TabIndex = 0;
            this.cell188.TabStop = false;
            // 
            // cell187
            // 
            this.cell187.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell187.Location = new System.Drawing.Point(389, 471);
            this.cell187.Name = "cell187";
            this.cell187.Size = new System.Drawing.Size(20, 20);
            this.cell187.TabIndex = 0;
            this.cell187.TabStop = false;
            // 
            // cell186
            // 
            this.cell186.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell186.Location = new System.Drawing.Point(355, 471);
            this.cell186.Name = "cell186";
            this.cell186.Size = new System.Drawing.Size(20, 20);
            this.cell186.TabIndex = 0;
            this.cell186.TabStop = false;
            // 
            // cell185
            // 
            this.cell185.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell185.Location = new System.Drawing.Point(321, 471);
            this.cell185.Name = "cell185";
            this.cell185.Size = new System.Drawing.Size(20, 20);
            this.cell185.TabIndex = 0;
            this.cell185.TabStop = false;
            // 
            // cell184
            // 
            this.cell184.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell184.Location = new System.Drawing.Point(287, 471);
            this.cell184.Name = "cell184";
            this.cell184.Size = new System.Drawing.Size(20, 20);
            this.cell184.TabIndex = 0;
            this.cell184.TabStop = false;
            // 
            // cell183
            // 
            this.cell183.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell183.Location = new System.Drawing.Point(253, 471);
            this.cell183.Name = "cell183";
            this.cell183.Size = new System.Drawing.Size(20, 20);
            this.cell183.TabIndex = 0;
            this.cell183.TabStop = false;
            // 
            // cell182
            // 
            this.cell182.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell182.Location = new System.Drawing.Point(219, 471);
            this.cell182.Name = "cell182";
            this.cell182.Size = new System.Drawing.Size(20, 20);
            this.cell182.TabIndex = 0;
            this.cell182.TabStop = false;
            // 
            // cell181
            // 
            this.cell181.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell181.Location = new System.Drawing.Point(185, 471);
            this.cell181.Name = "cell181";
            this.cell181.Size = new System.Drawing.Size(20, 20);
            this.cell181.TabIndex = 0;
            this.cell181.TabStop = false;
            // 
            // cell180
            // 
            this.cell180.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell180.Location = new System.Drawing.Point(151, 471);
            this.cell180.Name = "cell180";
            this.cell180.Size = new System.Drawing.Size(20, 20);
            this.cell180.TabIndex = 0;
            this.cell180.TabStop = false;
            // 
            // cell179
            // 
            this.cell179.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell179.Location = new System.Drawing.Point(117, 471);
            this.cell179.Name = "cell179";
            this.cell179.Size = new System.Drawing.Size(20, 20);
            this.cell179.TabIndex = 0;
            this.cell179.TabStop = false;
            // 
            // cell178
            // 
            this.cell178.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell178.Location = new System.Drawing.Point(83, 471);
            this.cell178.Name = "cell178";
            this.cell178.Size = new System.Drawing.Size(20, 20);
            this.cell178.TabIndex = 0;
            this.cell178.TabStop = false;
            // 
            // cell177
            // 
            this.cell177.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell177.Location = new System.Drawing.Point(48, 471);
            this.cell177.Name = "cell177";
            this.cell177.Size = new System.Drawing.Size(20, 20);
            this.cell177.TabIndex = 0;
            this.cell177.TabStop = false;
            // 
            // cell161
            // 
            this.cell161.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell161.Location = new System.Drawing.Point(48, 434);
            this.cell161.Name = "cell161";
            this.cell161.Size = new System.Drawing.Size(20, 20);
            this.cell161.TabIndex = 0;
            this.cell161.TabStop = false;
            // 
            // cell145
            // 
            this.cell145.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell145.Location = new System.Drawing.Point(48, 398);
            this.cell145.Name = "cell145";
            this.cell145.Size = new System.Drawing.Size(20, 20);
            this.cell145.TabIndex = 0;
            this.cell145.TabStop = false;
            // 
            // cell129
            // 
            this.cell129.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell129.Location = new System.Drawing.Point(48, 362);
            this.cell129.Name = "cell129";
            this.cell129.Size = new System.Drawing.Size(20, 20);
            this.cell129.TabIndex = 0;
            this.cell129.TabStop = false;
            // 
            // cell113
            // 
            this.cell113.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell113.Location = new System.Drawing.Point(48, 326);
            this.cell113.Name = "cell113";
            this.cell113.Size = new System.Drawing.Size(20, 20);
            this.cell113.TabIndex = 0;
            this.cell113.TabStop = false;
            // 
            // cell97
            // 
            this.cell97.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell97.Location = new System.Drawing.Point(48, 290);
            this.cell97.Name = "cell97";
            this.cell97.Size = new System.Drawing.Size(20, 20);
            this.cell97.TabIndex = 0;
            this.cell97.TabStop = false;
            // 
            // cell81
            // 
            this.cell81.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell81.Location = new System.Drawing.Point(48, 254);
            this.cell81.Name = "cell81";
            this.cell81.Size = new System.Drawing.Size(20, 20);
            this.cell81.TabIndex = 0;
            this.cell81.TabStop = false;
            // 
            // cell65
            // 
            this.cell65.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell65.Location = new System.Drawing.Point(48, 218);
            this.cell65.Name = "cell65";
            this.cell65.Size = new System.Drawing.Size(20, 20);
            this.cell65.TabIndex = 0;
            this.cell65.TabStop = false;
            // 
            // cell49
            // 
            this.cell49.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell49.Location = new System.Drawing.Point(48, 182);
            this.cell49.Name = "cell49";
            this.cell49.Size = new System.Drawing.Size(20, 20);
            this.cell49.TabIndex = 0;
            this.cell49.TabStop = false;
            // 
            // cell33
            // 
            this.cell33.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell33.Location = new System.Drawing.Point(48, 146);
            this.cell33.Name = "cell33";
            this.cell33.Size = new System.Drawing.Size(20, 20);
            this.cell33.TabIndex = 0;
            this.cell33.TabStop = false;
            // 
            // cell17
            // 
            this.cell17.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell17.Location = new System.Drawing.Point(48, 110);
            this.cell17.Name = "cell17";
            this.cell17.Size = new System.Drawing.Size(20, 20);
            this.cell17.TabIndex = 16;
            this.cell17.TabStop = false;
            // 
            // cell1
            // 
            this.cell1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cell1.Location = new System.Drawing.Point(48, 74);
            this.cell1.Name = "cell1";
            this.cell1.Size = new System.Drawing.Size(20, 20);
            this.cell1.TabIndex = 0;
            this.cell1.TabStop = false;
            // 
            // pointer1
            // 
            this.pointer1.BackColor = System.Drawing.Color.DarkRed;
            this.pointer1.Location = new System.Drawing.Point(48, 39);
            this.pointer1.Name = "pointer1";
            this.pointer1.Size = new System.Drawing.Size(20, 20);
            this.pointer1.TabIndex = 25;
            this.pointer1.TabStop = false;
            // 
            // pointer2
            // 
            this.pointer2.BackColor = System.Drawing.Color.DarkRed;
            this.pointer2.Location = new System.Drawing.Point(83, 39);
            this.pointer2.Name = "pointer2";
            this.pointer2.Size = new System.Drawing.Size(20, 20);
            this.pointer2.TabIndex = 26;
            this.pointer2.TabStop = false;
            // 
            // pointer3
            // 
            this.pointer3.BackColor = System.Drawing.Color.DarkRed;
            this.pointer3.Location = new System.Drawing.Point(117, 39);
            this.pointer3.Name = "pointer3";
            this.pointer3.Size = new System.Drawing.Size(20, 20);
            this.pointer3.TabIndex = 27;
            this.pointer3.TabStop = false;
            // 
            // pointer4
            // 
            this.pointer4.BackColor = System.Drawing.Color.DarkRed;
            this.pointer4.Location = new System.Drawing.Point(151, 39);
            this.pointer4.Name = "pointer4";
            this.pointer4.Size = new System.Drawing.Size(20, 20);
            this.pointer4.TabIndex = 27;
            this.pointer4.TabStop = false;
            // 
            // pointer5
            // 
            this.pointer5.BackColor = System.Drawing.Color.DarkRed;
            this.pointer5.Location = new System.Drawing.Point(185, 39);
            this.pointer5.Name = "pointer5";
            this.pointer5.Size = new System.Drawing.Size(20, 20);
            this.pointer5.TabIndex = 27;
            this.pointer5.TabStop = false;
            // 
            // pointer6
            // 
            this.pointer6.BackColor = System.Drawing.Color.DarkRed;
            this.pointer6.Location = new System.Drawing.Point(219, 39);
            this.pointer6.Name = "pointer6";
            this.pointer6.Size = new System.Drawing.Size(20, 20);
            this.pointer6.TabIndex = 27;
            this.pointer6.TabStop = false;
            // 
            // pointer7
            // 
            this.pointer7.BackColor = System.Drawing.Color.DarkRed;
            this.pointer7.Location = new System.Drawing.Point(253, 39);
            this.pointer7.Name = "pointer7";
            this.pointer7.Size = new System.Drawing.Size(20, 20);
            this.pointer7.TabIndex = 27;
            this.pointer7.TabStop = false;
            // 
            // pointer8
            // 
            this.pointer8.BackColor = System.Drawing.Color.DarkRed;
            this.pointer8.Location = new System.Drawing.Point(287, 39);
            this.pointer8.Name = "pointer8";
            this.pointer8.Size = new System.Drawing.Size(20, 20);
            this.pointer8.TabIndex = 27;
            this.pointer8.TabStop = false;
            // 
            // pointer9
            // 
            this.pointer9.BackColor = System.Drawing.Color.DarkRed;
            this.pointer9.Location = new System.Drawing.Point(321, 39);
            this.pointer9.Name = "pointer9";
            this.pointer9.Size = new System.Drawing.Size(20, 20);
            this.pointer9.TabIndex = 27;
            this.pointer9.TabStop = false;
            // 
            // pointer10
            // 
            this.pointer10.BackColor = System.Drawing.Color.DarkRed;
            this.pointer10.Location = new System.Drawing.Point(355, 39);
            this.pointer10.Name = "pointer10";
            this.pointer10.Size = new System.Drawing.Size(20, 20);
            this.pointer10.TabIndex = 27;
            this.pointer10.TabStop = false;
            // 
            // pointer11
            // 
            this.pointer11.BackColor = System.Drawing.Color.DarkRed;
            this.pointer11.Location = new System.Drawing.Point(389, 39);
            this.pointer11.Name = "pointer11";
            this.pointer11.Size = new System.Drawing.Size(20, 20);
            this.pointer11.TabIndex = 27;
            this.pointer11.TabStop = false;
            // 
            // pointer12
            // 
            this.pointer12.BackColor = System.Drawing.Color.DarkRed;
            this.pointer12.Location = new System.Drawing.Point(423, 39);
            this.pointer12.Name = "pointer12";
            this.pointer12.Size = new System.Drawing.Size(20, 20);
            this.pointer12.TabIndex = 27;
            this.pointer12.TabStop = false;
            // 
            // pointer13
            // 
            this.pointer13.BackColor = System.Drawing.Color.DarkRed;
            this.pointer13.Location = new System.Drawing.Point(457, 39);
            this.pointer13.Name = "pointer13";
            this.pointer13.Size = new System.Drawing.Size(20, 20);
            this.pointer13.TabIndex = 27;
            this.pointer13.TabStop = false;
            // 
            // pointer14
            // 
            this.pointer14.BackColor = System.Drawing.Color.DarkRed;
            this.pointer14.Location = new System.Drawing.Point(491, 39);
            this.pointer14.Name = "pointer14";
            this.pointer14.Size = new System.Drawing.Size(20, 20);
            this.pointer14.TabIndex = 27;
            this.pointer14.TabStop = false;
            // 
            // pointer15
            // 
            this.pointer15.BackColor = System.Drawing.Color.DarkRed;
            this.pointer15.Location = new System.Drawing.Point(525, 39);
            this.pointer15.Name = "pointer15";
            this.pointer15.Size = new System.Drawing.Size(20, 20);
            this.pointer15.TabIndex = 27;
            this.pointer15.TabStop = false;
            // 
            // pointer16
            // 
            this.pointer16.BackColor = System.Drawing.Color.DarkRed;
            this.pointer16.Location = new System.Drawing.Point(559, 39);
            this.pointer16.Name = "pointer16";
            this.pointer16.Size = new System.Drawing.Size(20, 20);
            this.pointer16.TabIndex = 27;
            this.pointer16.TabStop = false;
            // 
            // pointer17
            // 
            this.pointer17.BackColor = System.Drawing.Color.DarkRed;
            this.pointer17.Location = new System.Drawing.Point(48, 507);
            this.pointer17.Name = "pointer17";
            this.pointer17.Size = new System.Drawing.Size(20, 20);
            this.pointer17.TabIndex = 27;
            this.pointer17.TabStop = false;
            // 
            // pointer18
            // 
            this.pointer18.BackColor = System.Drawing.Color.DarkRed;
            this.pointer18.Location = new System.Drawing.Point(83, 507);
            this.pointer18.Name = "pointer18";
            this.pointer18.Size = new System.Drawing.Size(20, 20);
            this.pointer18.TabIndex = 27;
            this.pointer18.TabStop = false;
            // 
            // pointer19
            // 
            this.pointer19.BackColor = System.Drawing.Color.DarkRed;
            this.pointer19.Location = new System.Drawing.Point(117, 507);
            this.pointer19.Name = "pointer19";
            this.pointer19.Size = new System.Drawing.Size(20, 20);
            this.pointer19.TabIndex = 27;
            this.pointer19.TabStop = false;
            // 
            // pointer20
            // 
            this.pointer20.BackColor = System.Drawing.Color.DarkRed;
            this.pointer20.Location = new System.Drawing.Point(151, 507);
            this.pointer20.Name = "pointer20";
            this.pointer20.Size = new System.Drawing.Size(20, 20);
            this.pointer20.TabIndex = 27;
            this.pointer20.TabStop = false;
            // 
            // pointer21
            // 
            this.pointer21.BackColor = System.Drawing.Color.DarkRed;
            this.pointer21.Location = new System.Drawing.Point(185, 507);
            this.pointer21.Name = "pointer21";
            this.pointer21.Size = new System.Drawing.Size(20, 20);
            this.pointer21.TabIndex = 27;
            this.pointer21.TabStop = false;
            // 
            // pointer22
            // 
            this.pointer22.BackColor = System.Drawing.Color.DarkRed;
            this.pointer22.Location = new System.Drawing.Point(219, 507);
            this.pointer22.Name = "pointer22";
            this.pointer22.Size = new System.Drawing.Size(20, 20);
            this.pointer22.TabIndex = 27;
            this.pointer22.TabStop = false;
            // 
            // pointer23
            // 
            this.pointer23.BackColor = System.Drawing.Color.DarkRed;
            this.pointer23.Location = new System.Drawing.Point(253, 507);
            this.pointer23.Name = "pointer23";
            this.pointer23.Size = new System.Drawing.Size(20, 20);
            this.pointer23.TabIndex = 27;
            this.pointer23.TabStop = false;
            // 
            // pointer24
            // 
            this.pointer24.BackColor = System.Drawing.Color.DarkRed;
            this.pointer24.Location = new System.Drawing.Point(287, 507);
            this.pointer24.Name = "pointer24";
            this.pointer24.Size = new System.Drawing.Size(20, 20);
            this.pointer24.TabIndex = 27;
            this.pointer24.TabStop = false;
            // 
            // pointer25
            // 
            this.pointer25.BackColor = System.Drawing.Color.DarkRed;
            this.pointer25.Location = new System.Drawing.Point(321, 507);
            this.pointer25.Name = "pointer25";
            this.pointer25.Size = new System.Drawing.Size(20, 20);
            this.pointer25.TabIndex = 27;
            this.pointer25.TabStop = false;
            // 
            // pointer26
            // 
            this.pointer26.BackColor = System.Drawing.Color.DarkRed;
            this.pointer26.Location = new System.Drawing.Point(355, 507);
            this.pointer26.Name = "pointer26";
            this.pointer26.Size = new System.Drawing.Size(20, 20);
            this.pointer26.TabIndex = 27;
            this.pointer26.TabStop = false;
            // 
            // pointer27
            // 
            this.pointer27.BackColor = System.Drawing.Color.DarkRed;
            this.pointer27.Location = new System.Drawing.Point(389, 507);
            this.pointer27.Name = "pointer27";
            this.pointer27.Size = new System.Drawing.Size(20, 20);
            this.pointer27.TabIndex = 27;
            this.pointer27.TabStop = false;
            // 
            // pointer28
            // 
            this.pointer28.BackColor = System.Drawing.Color.DarkRed;
            this.pointer28.Location = new System.Drawing.Point(423, 507);
            this.pointer28.Name = "pointer28";
            this.pointer28.Size = new System.Drawing.Size(20, 20);
            this.pointer28.TabIndex = 27;
            this.pointer28.TabStop = false;
            // 
            // pointer29
            // 
            this.pointer29.BackColor = System.Drawing.Color.DarkRed;
            this.pointer29.Location = new System.Drawing.Point(457, 507);
            this.pointer29.Name = "pointer29";
            this.pointer29.Size = new System.Drawing.Size(20, 20);
            this.pointer29.TabIndex = 27;
            this.pointer29.TabStop = false;
            // 
            // pointer30
            // 
            this.pointer30.BackColor = System.Drawing.Color.DarkRed;
            this.pointer30.Location = new System.Drawing.Point(491, 507);
            this.pointer30.Name = "pointer30";
            this.pointer30.Size = new System.Drawing.Size(20, 20);
            this.pointer30.TabIndex = 27;
            this.pointer30.TabStop = false;
            // 
            // pointer31
            // 
            this.pointer31.BackColor = System.Drawing.Color.DarkRed;
            this.pointer31.Location = new System.Drawing.Point(525, 507);
            this.pointer31.Name = "pointer31";
            this.pointer31.Size = new System.Drawing.Size(20, 20);
            this.pointer31.TabIndex = 27;
            this.pointer31.TabStop = false;
            // 
            // pointer32
            // 
            this.pointer32.BackColor = System.Drawing.Color.DarkRed;
            this.pointer32.Location = new System.Drawing.Point(559, 507);
            this.pointer32.Name = "pointer32";
            this.pointer32.Size = new System.Drawing.Size(20, 20);
            this.pointer32.TabIndex = 27;
            this.pointer32.TabStop = false;
            // 
            // beatTicker
            // 
            this.beatTicker.Enabled = true;
            this.beatTicker.Interval = 500;
            this.beatTicker.Tick += new System.EventHandler(this.HeartBeat);
            // 
            // speed
            // 
            this.speed.Location = new System.Drawing.Point(609, 316);
            this.speed.Maximum = 1000;
            this.speed.Minimum = 10;
            this.speed.Name = "speed";
            this.speed.Size = new System.Drawing.Size(205, 45);
            this.speed.TabIndex = 28;
            this.speed.Value = 500;
            this.speed.ValueChanged += new System.EventHandler(this.speed_ValueChanged);
            // 
            // releaseDial
            // 
            this.releaseDial.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.releaseDial.ForeColor = System.Drawing.Color.Goldenrod;
            this.releaseDial.KnobBorderStyle = AuSharp.KnobStyle.FlatBorder;
            this.releaseDial.KnobColor = System.Drawing.Color.Brown;
            this.releaseDial.KnobRadius = 20;
            this.releaseDial.Location = new System.Drawing.Point(609, 229);
            this.releaseDial.MarkerColor = System.Drawing.Color.Chocolate;
            this.releaseDial.Name = "releaseDial";
            this.releaseDial.Size = new System.Drawing.Size(75, 81);
            this.releaseDial.TabIndex = 29;
            this.releaseDial.Text = "Release";
            this.releaseDial.TextKnobRelation = AuSharp.TextKnobRelation.KnobAboveText;
            this.releaseDial.TickColor = System.Drawing.Color.BlanchedAlmond;
            // 
            // knob1
            // 
            this.knob1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.knob1.ForeColor = System.Drawing.Color.Goldenrod;
            this.knob1.KnobBorderStyle = AuSharp.KnobStyle.FlatBorder;
            this.knob1.KnobColor = System.Drawing.Color.Brown;
            this.knob1.KnobRadius = 20;
            this.knob1.Location = new System.Drawing.Point(673, 158);
            this.knob1.MarkerColor = System.Drawing.Color.Chocolate;
            this.knob1.Name = "knob1";
            this.knob1.Size = new System.Drawing.Size(75, 81);
            this.knob1.TabIndex = 30;
            this.knob1.Text = "Volume";
            this.knob1.TextKnobRelation = AuSharp.TextKnobRelation.KnobAboveText;
            this.knob1.TickColor = System.Drawing.Color.BlanchedAlmond;
            // 
            // waveShapeDial
            // 
            this.waveShapeDial.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waveShapeDial.ForeColor = System.Drawing.Color.Goldenrod;
            this.waveShapeDial.KnobBorderStyle = AuSharp.KnobStyle.FlatBorder;
            this.waveShapeDial.KnobColor = System.Drawing.Color.Brown;
            this.waveShapeDial.KnobRadius = 20;
            this.waveShapeDial.Location = new System.Drawing.Point(739, 229);
            this.waveShapeDial.MarkerColor = System.Drawing.Color.Chocolate;
            this.waveShapeDial.Name = "waveShapeDial";
            this.waveShapeDial.Size = new System.Drawing.Size(75, 81);
            this.waveShapeDial.TabIndex = 31;
            this.waveShapeDial.Text = "Wave Shape";
            this.waveShapeDial.TextKnobRelation = AuSharp.TextKnobRelation.KnobAboveText;
            this.waveShapeDial.TickColor = System.Drawing.Color.BlanchedAlmond;
            // 
            // ledBulb1
            // 
            this.ledBulb1.Color = System.Drawing.Color.LightGreen;
            this.ledBulb1.Location = new System.Drawing.Point(28, 79);
            this.ledBulb1.Name = "ledBulb1";
            this.ledBulb1.On = true;
            this.ledBulb1.Size = new System.Drawing.Size(10, 10);
            this.ledBulb1.TabIndex = 33;
            this.ledBulb1.Text = "ledBulb2";
            // 
            // ledBulb2
            // 
            this.ledBulb2.Color = System.Drawing.Color.LightGreen;
            this.ledBulb2.Location = new System.Drawing.Point(28, 114);
            this.ledBulb2.Name = "ledBulb2";
            this.ledBulb2.On = true;
            this.ledBulb2.Size = new System.Drawing.Size(10, 10);
            this.ledBulb2.TabIndex = 33;
            this.ledBulb2.Text = "ledBulb2";
            // 
            // ledBulb3
            // 
            this.ledBulb3.Color = System.Drawing.Color.LightGreen;
            this.ledBulb3.Location = new System.Drawing.Point(28, 151);
            this.ledBulb3.Name = "ledBulb3";
            this.ledBulb3.On = true;
            this.ledBulb3.Size = new System.Drawing.Size(10, 10);
            this.ledBulb3.TabIndex = 34;
            this.ledBulb3.Text = "ledBulb3";
            // 
            // ledBulb4
            // 
            this.ledBulb4.Color = System.Drawing.Color.LightGreen;
            this.ledBulb4.Location = new System.Drawing.Point(28, 187);
            this.ledBulb4.Name = "ledBulb4";
            this.ledBulb4.On = true;
            this.ledBulb4.Size = new System.Drawing.Size(10, 10);
            this.ledBulb4.TabIndex = 35;
            this.ledBulb4.Text = "ledBulb4";
            // 
            // ledBulb5
            // 
            this.ledBulb5.Color = System.Drawing.Color.LightGreen;
            this.ledBulb5.Location = new System.Drawing.Point(28, 223);
            this.ledBulb5.Name = "ledBulb5";
            this.ledBulb5.On = true;
            this.ledBulb5.Size = new System.Drawing.Size(10, 10);
            this.ledBulb5.TabIndex = 36;
            this.ledBulb5.Text = "ledBulb5";
            // 
            // ledBulb6
            // 
            this.ledBulb6.Color = System.Drawing.Color.LightGreen;
            this.ledBulb6.Location = new System.Drawing.Point(28, 259);
            this.ledBulb6.Name = "ledBulb6";
            this.ledBulb6.On = true;
            this.ledBulb6.Size = new System.Drawing.Size(10, 10);
            this.ledBulb6.TabIndex = 37;
            this.ledBulb6.Text = "ledBulb6";
            // 
            // ledBulb7
            // 
            this.ledBulb7.Color = System.Drawing.Color.LightGreen;
            this.ledBulb7.Location = new System.Drawing.Point(28, 295);
            this.ledBulb7.Name = "ledBulb7";
            this.ledBulb7.On = true;
            this.ledBulb7.Size = new System.Drawing.Size(10, 10);
            this.ledBulb7.TabIndex = 38;
            this.ledBulb7.Text = "ledBulb7";
            // 
            // ledBulb8
            // 
            this.ledBulb8.Color = System.Drawing.Color.LightGreen;
            this.ledBulb8.Location = new System.Drawing.Point(28, 331);
            this.ledBulb8.Name = "ledBulb8";
            this.ledBulb8.On = true;
            this.ledBulb8.Size = new System.Drawing.Size(10, 10);
            this.ledBulb8.TabIndex = 39;
            this.ledBulb8.Text = "ledBulb8";
            // 
            // ledBulb9
            // 
            this.ledBulb9.Color = System.Drawing.Color.LightGreen;
            this.ledBulb9.Location = new System.Drawing.Point(28, 367);
            this.ledBulb9.Name = "ledBulb9";
            this.ledBulb9.On = true;
            this.ledBulb9.Size = new System.Drawing.Size(10, 10);
            this.ledBulb9.TabIndex = 40;
            this.ledBulb9.Text = "ledBulb9";
            // 
            // ledBulb10
            // 
            this.ledBulb10.Color = System.Drawing.Color.LightGreen;
            this.ledBulb10.Location = new System.Drawing.Point(28, 403);
            this.ledBulb10.Name = "ledBulb10";
            this.ledBulb10.On = true;
            this.ledBulb10.Size = new System.Drawing.Size(10, 10);
            this.ledBulb10.TabIndex = 41;
            this.ledBulb10.Text = "ledBulb10";
            // 
            // ledBulb11
            // 
            this.ledBulb11.Color = System.Drawing.Color.LightGreen;
            this.ledBulb11.Location = new System.Drawing.Point(28, 439);
            this.ledBulb11.Name = "ledBulb11";
            this.ledBulb11.On = true;
            this.ledBulb11.Size = new System.Drawing.Size(10, 10);
            this.ledBulb11.TabIndex = 42;
            this.ledBulb11.Text = "ledBulb11";
            // 
            // ledBulb12
            // 
            this.ledBulb12.Color = System.Drawing.Color.LightGreen;
            this.ledBulb12.Location = new System.Drawing.Point(28, 476);
            this.ledBulb12.Name = "ledBulb12";
            this.ledBulb12.On = true;
            this.ledBulb12.Size = new System.Drawing.Size(10, 10);
            this.ledBulb12.TabIndex = 43;
            this.ledBulb12.Text = "ledBulb12";
            // 
            // StepSequencer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.ClientSize = new System.Drawing.Size(834, 561);
            this.Controls.Add(this.ledBulb12);
            this.Controls.Add(this.ledBulb11);
            this.Controls.Add(this.ledBulb10);
            this.Controls.Add(this.ledBulb9);
            this.Controls.Add(this.ledBulb8);
            this.Controls.Add(this.ledBulb7);
            this.Controls.Add(this.ledBulb6);
            this.Controls.Add(this.ledBulb5);
            this.Controls.Add(this.ledBulb4);
            this.Controls.Add(this.ledBulb3);
            this.Controls.Add(this.ledBulb2);
            this.Controls.Add(this.ledBulb1);
            this.Controls.Add(this.waveShapeDial);
            this.Controls.Add(this.knob1);
            this.Controls.Add(this.releaseDial);
            this.Controls.Add(this.speed);
            this.Controls.Add(this.pointer32);
            this.Controls.Add(this.pointer31);
            this.Controls.Add(this.pointer30);
            this.Controls.Add(this.pointer29);
            this.Controls.Add(this.pointer28);
            this.Controls.Add(this.pointer27);
            this.Controls.Add(this.pointer26);
            this.Controls.Add(this.pointer25);
            this.Controls.Add(this.pointer24);
            this.Controls.Add(this.pointer23);
            this.Controls.Add(this.pointer22);
            this.Controls.Add(this.pointer21);
            this.Controls.Add(this.pointer20);
            this.Controls.Add(this.pointer19);
            this.Controls.Add(this.pointer18);
            this.Controls.Add(this.pointer17);
            this.Controls.Add(this.pointer16);
            this.Controls.Add(this.pointer15);
            this.Controls.Add(this.pointer14);
            this.Controls.Add(this.pointer13);
            this.Controls.Add(this.pointer12);
            this.Controls.Add(this.pointer11);
            this.Controls.Add(this.pointer10);
            this.Controls.Add(this.pointer9);
            this.Controls.Add(this.pointer8);
            this.Controls.Add(this.pointer7);
            this.Controls.Add(this.pointer6);
            this.Controls.Add(this.pointer5);
            this.Controls.Add(this.pointer4);
            this.Controls.Add(this.pointer3);
            this.Controls.Add(this.pointer2);
            this.Controls.Add(this.pointer1);
            this.Controls.Add(this.cell7);
            this.Controls.Add(this.cell8);
            this.Controls.Add(this.cell16);
            this.Controls.Add(this.cell15);
            this.Controls.Add(this.cell9);
            this.Controls.Add(this.cell14);
            this.Controls.Add(this.cell10);
            this.Controls.Add(this.cell13);
            this.Controls.Add(this.cell11);
            this.Controls.Add(this.cell12);
            this.Controls.Add(this.cell4);
            this.Controls.Add(this.cell5);
            this.Controls.Add(this.cell6);
            this.Controls.Add(this.cell3);
            this.Controls.Add(this.cell2);
            this.Controls.Add(this.cell32);
            this.Controls.Add(this.cell31);
            this.Controls.Add(this.cell30);
            this.Controls.Add(this.cell29);
            this.Controls.Add(this.cell28);
            this.Controls.Add(this.cell27);
            this.Controls.Add(this.cell26);
            this.Controls.Add(this.cell25);
            this.Controls.Add(this.cell24);
            this.Controls.Add(this.cell23);
            this.Controls.Add(this.cell22);
            this.Controls.Add(this.cell21);
            this.Controls.Add(this.cell20);
            this.Controls.Add(this.cell19);
            this.Controls.Add(this.cell18);
            this.Controls.Add(this.cell48);
            this.Controls.Add(this.cell47);
            this.Controls.Add(this.cell46);
            this.Controls.Add(this.cell45);
            this.Controls.Add(this.cell44);
            this.Controls.Add(this.cell43);
            this.Controls.Add(this.cell42);
            this.Controls.Add(this.cell41);
            this.Controls.Add(this.cell40);
            this.Controls.Add(this.cell39);
            this.Controls.Add(this.cell38);
            this.Controls.Add(this.cell37);
            this.Controls.Add(this.cell36);
            this.Controls.Add(this.cell35);
            this.Controls.Add(this.cell34);
            this.Controls.Add(this.cell64);
            this.Controls.Add(this.cell63);
            this.Controls.Add(this.cell62);
            this.Controls.Add(this.cell61);
            this.Controls.Add(this.cell60);
            this.Controls.Add(this.cell59);
            this.Controls.Add(this.cell58);
            this.Controls.Add(this.cell57);
            this.Controls.Add(this.cell56);
            this.Controls.Add(this.cell55);
            this.Controls.Add(this.cell54);
            this.Controls.Add(this.cell53);
            this.Controls.Add(this.cell52);
            this.Controls.Add(this.cell51);
            this.Controls.Add(this.cell50);
            this.Controls.Add(this.cell80);
            this.Controls.Add(this.cell79);
            this.Controls.Add(this.cell78);
            this.Controls.Add(this.cell77);
            this.Controls.Add(this.cell76);
            this.Controls.Add(this.cell75);
            this.Controls.Add(this.cell74);
            this.Controls.Add(this.cell73);
            this.Controls.Add(this.cell72);
            this.Controls.Add(this.cell71);
            this.Controls.Add(this.cell70);
            this.Controls.Add(this.cell69);
            this.Controls.Add(this.cell68);
            this.Controls.Add(this.cell67);
            this.Controls.Add(this.cell66);
            this.Controls.Add(this.cell96);
            this.Controls.Add(this.cell95);
            this.Controls.Add(this.cell94);
            this.Controls.Add(this.cell93);
            this.Controls.Add(this.cell92);
            this.Controls.Add(this.cell91);
            this.Controls.Add(this.cell90);
            this.Controls.Add(this.cell89);
            this.Controls.Add(this.cell88);
            this.Controls.Add(this.cell87);
            this.Controls.Add(this.cell86);
            this.Controls.Add(this.cell85);
            this.Controls.Add(this.cell84);
            this.Controls.Add(this.cell83);
            this.Controls.Add(this.cell82);
            this.Controls.Add(this.cell112);
            this.Controls.Add(this.cell111);
            this.Controls.Add(this.cell110);
            this.Controls.Add(this.cell109);
            this.Controls.Add(this.cell108);
            this.Controls.Add(this.cell107);
            this.Controls.Add(this.cell106);
            this.Controls.Add(this.cell105);
            this.Controls.Add(this.cell104);
            this.Controls.Add(this.cell103);
            this.Controls.Add(this.cell102);
            this.Controls.Add(this.cell101);
            this.Controls.Add(this.cell100);
            this.Controls.Add(this.cell99);
            this.Controls.Add(this.cell98);
            this.Controls.Add(this.cell128);
            this.Controls.Add(this.cell127);
            this.Controls.Add(this.cell126);
            this.Controls.Add(this.cell125);
            this.Controls.Add(this.cell124);
            this.Controls.Add(this.cell123);
            this.Controls.Add(this.cell122);
            this.Controls.Add(this.cell121);
            this.Controls.Add(this.cell120);
            this.Controls.Add(this.cell119);
            this.Controls.Add(this.cell118);
            this.Controls.Add(this.cell117);
            this.Controls.Add(this.cell116);
            this.Controls.Add(this.cell115);
            this.Controls.Add(this.cell114);
            this.Controls.Add(this.cell144);
            this.Controls.Add(this.cell143);
            this.Controls.Add(this.cell142);
            this.Controls.Add(this.cell141);
            this.Controls.Add(this.cell140);
            this.Controls.Add(this.cell139);
            this.Controls.Add(this.cell138);
            this.Controls.Add(this.cell137);
            this.Controls.Add(this.cell136);
            this.Controls.Add(this.cell135);
            this.Controls.Add(this.cell134);
            this.Controls.Add(this.cell133);
            this.Controls.Add(this.cell132);
            this.Controls.Add(this.cell131);
            this.Controls.Add(this.cell130);
            this.Controls.Add(this.cell160);
            this.Controls.Add(this.cell159);
            this.Controls.Add(this.cell158);
            this.Controls.Add(this.cell157);
            this.Controls.Add(this.cell156);
            this.Controls.Add(this.cell155);
            this.Controls.Add(this.cell154);
            this.Controls.Add(this.cell153);
            this.Controls.Add(this.cell152);
            this.Controls.Add(this.cell151);
            this.Controls.Add(this.cell150);
            this.Controls.Add(this.cell149);
            this.Controls.Add(this.cell148);
            this.Controls.Add(this.cell147);
            this.Controls.Add(this.cell146);
            this.Controls.Add(this.cell176);
            this.Controls.Add(this.cell175);
            this.Controls.Add(this.cell174);
            this.Controls.Add(this.cell173);
            this.Controls.Add(this.cell172);
            this.Controls.Add(this.cell171);
            this.Controls.Add(this.cell170);
            this.Controls.Add(this.cell169);
            this.Controls.Add(this.cell168);
            this.Controls.Add(this.cell167);
            this.Controls.Add(this.cell166);
            this.Controls.Add(this.cell165);
            this.Controls.Add(this.cell164);
            this.Controls.Add(this.cell163);
            this.Controls.Add(this.cell162);
            this.Controls.Add(this.cell192);
            this.Controls.Add(this.cell191);
            this.Controls.Add(this.cell190);
            this.Controls.Add(this.cell189);
            this.Controls.Add(this.cell188);
            this.Controls.Add(this.cell187);
            this.Controls.Add(this.cell186);
            this.Controls.Add(this.cell185);
            this.Controls.Add(this.cell184);
            this.Controls.Add(this.cell183);
            this.Controls.Add(this.cell182);
            this.Controls.Add(this.cell181);
            this.Controls.Add(this.cell180);
            this.Controls.Add(this.cell179);
            this.Controls.Add(this.cell178);
            this.Controls.Add(this.cell177);
            this.Controls.Add(this.cell161);
            this.Controls.Add(this.cell145);
            this.Controls.Add(this.cell129);
            this.Controls.Add(this.cell113);
            this.Controls.Add(this.cell97);
            this.Controls.Add(this.cell81);
            this.Controls.Add(this.cell65);
            this.Controls.Add(this.cell49);
            this.Controls.Add(this.cell33);
            this.Controls.Add(this.cell17);
            this.Controls.Add(this.cell1);
            this.Name = "StepSequencer";
            this.Text = " ";
            this.Load += new System.EventHandler(FmodUtilities.OnFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.cell7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell176)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell175)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell174)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell173)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell172)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell192)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell191)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell190)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell189)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell187)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell186)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell185)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell181)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell180)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell179)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell178)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell177)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cell1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointer32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox cell1;
        private System.Windows.Forms.PictureBox cell2;
        private System.Windows.Forms.PictureBox cell3;
        private System.Windows.Forms.PictureBox cell4;
        private System.Windows.Forms.PictureBox cell5;
        private System.Windows.Forms.PictureBox cell6;
        private System.Windows.Forms.PictureBox cell7;
        private System.Windows.Forms.PictureBox cell8;
        private System.Windows.Forms.PictureBox cell9;
        private System.Windows.Forms.PictureBox cell10;
        private System.Windows.Forms.PictureBox cell11;
        private System.Windows.Forms.PictureBox cell12;
        private System.Windows.Forms.PictureBox cell13;
        private System.Windows.Forms.PictureBox cell14;
        private System.Windows.Forms.PictureBox cell15;
        private System.Windows.Forms.PictureBox cell16;
        private System.Windows.Forms.PictureBox cell17;
        private System.Windows.Forms.PictureBox cell18;
        private System.Windows.Forms.PictureBox cell19;
        private System.Windows.Forms.PictureBox cell20;
        private System.Windows.Forms.PictureBox cell21;
        private System.Windows.Forms.PictureBox cell22;
        private System.Windows.Forms.PictureBox cell23;
        private System.Windows.Forms.PictureBox cell24;
        private System.Windows.Forms.PictureBox cell25;
        private System.Windows.Forms.PictureBox cell26;
        private System.Windows.Forms.PictureBox cell27;
        private System.Windows.Forms.PictureBox cell28;
        private System.Windows.Forms.PictureBox cell29;
        private System.Windows.Forms.PictureBox cell30;
        private System.Windows.Forms.PictureBox cell31;
        private System.Windows.Forms.PictureBox cell32;
        private System.Windows.Forms.PictureBox cell33;
        private System.Windows.Forms.PictureBox cell34;
        private System.Windows.Forms.PictureBox cell35;
        private System.Windows.Forms.PictureBox cell36;
        private System.Windows.Forms.PictureBox cell37;
        private System.Windows.Forms.PictureBox cell38;
        private System.Windows.Forms.PictureBox cell39;
        private System.Windows.Forms.PictureBox cell40;
        private System.Windows.Forms.PictureBox cell41;
        private System.Windows.Forms.PictureBox cell42;
        private System.Windows.Forms.PictureBox cell43;
        private System.Windows.Forms.PictureBox cell44;
        private System.Windows.Forms.PictureBox cell45;
        private System.Windows.Forms.PictureBox cell46;
        private System.Windows.Forms.PictureBox cell47;
        private System.Windows.Forms.PictureBox cell48;
        private System.Windows.Forms.PictureBox cell49;
        private System.Windows.Forms.PictureBox cell50;
        private System.Windows.Forms.PictureBox cell51;
        private System.Windows.Forms.PictureBox cell52;
        private System.Windows.Forms.PictureBox cell53;
        private System.Windows.Forms.PictureBox cell54;
        private System.Windows.Forms.PictureBox cell55;
        private System.Windows.Forms.PictureBox cell56;
        private System.Windows.Forms.PictureBox cell57;
        private System.Windows.Forms.PictureBox cell58;
        private System.Windows.Forms.PictureBox cell59;
        private System.Windows.Forms.PictureBox cell60;
        private System.Windows.Forms.PictureBox cell61;
        private System.Windows.Forms.PictureBox cell62;
        private System.Windows.Forms.PictureBox cell63;
        private System.Windows.Forms.PictureBox cell64;
        private System.Windows.Forms.PictureBox cell65;
        private System.Windows.Forms.PictureBox cell66;
        private System.Windows.Forms.PictureBox cell67;
        private System.Windows.Forms.PictureBox cell68;
        private System.Windows.Forms.PictureBox cell69;
        private System.Windows.Forms.PictureBox cell70;
        private System.Windows.Forms.PictureBox cell71;
        private System.Windows.Forms.PictureBox cell72;
        private System.Windows.Forms.PictureBox cell73;
        private System.Windows.Forms.PictureBox cell74;
        private System.Windows.Forms.PictureBox cell75;
        private System.Windows.Forms.PictureBox cell76;
        private System.Windows.Forms.PictureBox cell77;
        private System.Windows.Forms.PictureBox cell78;
        private System.Windows.Forms.PictureBox cell79;
        private System.Windows.Forms.PictureBox cell80;
        private System.Windows.Forms.PictureBox cell81;
        private System.Windows.Forms.PictureBox cell82;
        private System.Windows.Forms.PictureBox cell83;
        private System.Windows.Forms.PictureBox cell84;
        private System.Windows.Forms.PictureBox cell85;
        private System.Windows.Forms.PictureBox cell86;
        private System.Windows.Forms.PictureBox cell87;
        private System.Windows.Forms.PictureBox cell88;
        private System.Windows.Forms.PictureBox cell89;
        private System.Windows.Forms.PictureBox cell90;
        private System.Windows.Forms.PictureBox cell91;
        private System.Windows.Forms.PictureBox cell92;
        private System.Windows.Forms.PictureBox cell93;
        private System.Windows.Forms.PictureBox cell94;
        private System.Windows.Forms.PictureBox cell95;
        private System.Windows.Forms.PictureBox cell96;
        private System.Windows.Forms.PictureBox cell97;
        private System.Windows.Forms.PictureBox cell98;
        private System.Windows.Forms.PictureBox cell99;
        private System.Windows.Forms.PictureBox cell100;
        private System.Windows.Forms.PictureBox cell101;
        private System.Windows.Forms.PictureBox cell102;
        private System.Windows.Forms.PictureBox cell103;
        private System.Windows.Forms.PictureBox cell104;
        private System.Windows.Forms.PictureBox cell105;
        private System.Windows.Forms.PictureBox cell106;
        private System.Windows.Forms.PictureBox cell107;
        private System.Windows.Forms.PictureBox cell108;
        private System.Windows.Forms.PictureBox cell109;
        private System.Windows.Forms.PictureBox cell110;
        private System.Windows.Forms.PictureBox cell111;
        private System.Windows.Forms.PictureBox cell112;
        private System.Windows.Forms.PictureBox cell113;
        private System.Windows.Forms.PictureBox cell114;
        private System.Windows.Forms.PictureBox cell115;
        private System.Windows.Forms.PictureBox cell116;
        private System.Windows.Forms.PictureBox cell117;
        private System.Windows.Forms.PictureBox cell118;
        private System.Windows.Forms.PictureBox cell119;
        private System.Windows.Forms.PictureBox cell120;
        private System.Windows.Forms.PictureBox cell121;
        private System.Windows.Forms.PictureBox cell122;
        private System.Windows.Forms.PictureBox cell123;
        private System.Windows.Forms.PictureBox cell124;
        private System.Windows.Forms.PictureBox cell125;
        private System.Windows.Forms.PictureBox cell126;
        private System.Windows.Forms.PictureBox cell127;
        private System.Windows.Forms.PictureBox cell128;
        private System.Windows.Forms.PictureBox cell129;
        private System.Windows.Forms.PictureBox cell130;
        private System.Windows.Forms.PictureBox cell131;
        private System.Windows.Forms.PictureBox cell132;
        private System.Windows.Forms.PictureBox cell133;
        private System.Windows.Forms.PictureBox cell134;
        private System.Windows.Forms.PictureBox cell135;
        private System.Windows.Forms.PictureBox cell136;
        private System.Windows.Forms.PictureBox cell137;
        private System.Windows.Forms.PictureBox cell138;
        private System.Windows.Forms.PictureBox cell139;
        private System.Windows.Forms.PictureBox cell140;
        private System.Windows.Forms.PictureBox cell141;
        private System.Windows.Forms.PictureBox cell142;
        private System.Windows.Forms.PictureBox cell143;
        private System.Windows.Forms.PictureBox cell144;
        private System.Windows.Forms.PictureBox cell145;
        private System.Windows.Forms.PictureBox cell146;
        private System.Windows.Forms.PictureBox cell147;
        private System.Windows.Forms.PictureBox cell148;
        private System.Windows.Forms.PictureBox cell149;
        private System.Windows.Forms.PictureBox cell150;
        private System.Windows.Forms.PictureBox cell151;
        private System.Windows.Forms.PictureBox cell152;
        private System.Windows.Forms.PictureBox cell153;
        private System.Windows.Forms.PictureBox cell154;
        private System.Windows.Forms.PictureBox cell155;
        private System.Windows.Forms.PictureBox cell156;
        private System.Windows.Forms.PictureBox cell157;
        private System.Windows.Forms.PictureBox cell158;
        private System.Windows.Forms.PictureBox cell159;
        private System.Windows.Forms.PictureBox cell160;
        private System.Windows.Forms.PictureBox cell161;
        private System.Windows.Forms.PictureBox cell162;
        private System.Windows.Forms.PictureBox cell163;
        private System.Windows.Forms.PictureBox cell164;
        private System.Windows.Forms.PictureBox cell165;
        private System.Windows.Forms.PictureBox cell166;
        private System.Windows.Forms.PictureBox cell167;
        private System.Windows.Forms.PictureBox cell168;
        private System.Windows.Forms.PictureBox cell169;
        private System.Windows.Forms.PictureBox cell170;
        private System.Windows.Forms.PictureBox cell171;
        private System.Windows.Forms.PictureBox cell172;
        private System.Windows.Forms.PictureBox cell173;
        private System.Windows.Forms.PictureBox cell174;
        private System.Windows.Forms.PictureBox cell175;
        private System.Windows.Forms.PictureBox cell176;
        private System.Windows.Forms.PictureBox cell177;
        private System.Windows.Forms.PictureBox cell178;
        private System.Windows.Forms.PictureBox cell179;
        private System.Windows.Forms.PictureBox cell180;
        private System.Windows.Forms.PictureBox cell181;
        private System.Windows.Forms.PictureBox cell182;
        private System.Windows.Forms.PictureBox cell183;
        private System.Windows.Forms.PictureBox cell184;
        private System.Windows.Forms.PictureBox cell185;
        private System.Windows.Forms.PictureBox cell186;
        private System.Windows.Forms.PictureBox cell187;
        private System.Windows.Forms.PictureBox cell188;
        private System.Windows.Forms.PictureBox cell189;
        private System.Windows.Forms.PictureBox cell190;
        private System.Windows.Forms.PictureBox cell191;
        private System.Windows.Forms.PictureBox cell192;
        private System.Windows.Forms.PictureBox pointer1;
        private System.Windows.Forms.PictureBox pointer2;
        private System.Windows.Forms.PictureBox pointer3;
        private System.Windows.Forms.PictureBox pointer4;
        private System.Windows.Forms.PictureBox pointer5;
        private System.Windows.Forms.PictureBox pointer6;
        private System.Windows.Forms.PictureBox pointer7;
        private System.Windows.Forms.PictureBox pointer8;
        private System.Windows.Forms.PictureBox pointer9;
        private System.Windows.Forms.PictureBox pointer10;
        private System.Windows.Forms.PictureBox pointer11;
        private System.Windows.Forms.PictureBox pointer12;
        private System.Windows.Forms.PictureBox pointer13;
        private System.Windows.Forms.PictureBox pointer14;
        private System.Windows.Forms.PictureBox pointer15;
        private System.Windows.Forms.PictureBox pointer16;
        private System.Windows.Forms.PictureBox pointer17;
        private System.Windows.Forms.PictureBox pointer18;
        private System.Windows.Forms.PictureBox pointer19;
        private System.Windows.Forms.PictureBox pointer20;
        private System.Windows.Forms.PictureBox pointer21;
        private System.Windows.Forms.PictureBox pointer22;
        private System.Windows.Forms.PictureBox pointer23;
        private System.Windows.Forms.PictureBox pointer24;
        private System.Windows.Forms.PictureBox pointer25;
        private System.Windows.Forms.PictureBox pointer26;
        private System.Windows.Forms.PictureBox pointer27;
        private System.Windows.Forms.PictureBox pointer28;
        private System.Windows.Forms.PictureBox pointer29;
        private System.Windows.Forms.PictureBox pointer30;
        private System.Windows.Forms.PictureBox pointer31;
        private System.Windows.Forms.PictureBox pointer32;
        private System.Windows.Forms.Timer beatTicker;
        private System.Windows.Forms.TrackBar speed;
        private AuSharp.Knob releaseDial;
        private AuSharp.Knob knob1;
        private AuSharp.Knob waveShapeDial;
        private LedBulb ledBulb1;
        private LedBulb ledBulb2;
        private LedBulb ledBulb3;
        private LedBulb ledBulb4;
        private LedBulb ledBulb5;
        private LedBulb ledBulb6;
        private LedBulb ledBulb7;
        private LedBulb ledBulb8;
        private LedBulb ledBulb9;
        private LedBulb ledBulb10;
        private LedBulb ledBulb11;
        private LedBulb ledBulb12;
    }
}

